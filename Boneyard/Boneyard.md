# The Boneyard
The boneyard is where ideas go to die.

These are game elements which may be included later but which I will probably never actually get to.


## Types of Damage

* Physical - clubs, gun and grenades all do physical damage
* Energy - Lazers, tesla coils, and flamethrowers all do energy damage. Note: Defining “fire damage”
does not automatically catch things on fire. That would be an added effect purchased separately.

* Psyonic? - Hard to translate to physical damage unless we say what the mind believes happens.



This stuff is from when I thought it was going to be a bit closer to Warhammer Fantasy RolePlay (but in the future)
# Characters, Careers and Classes

Despite there being multiple intelligent races we do not see the others that frequntly.
Our planet is considered kind of a crime ridden dump.

On Earth the social structure has returned to an era of a small number of very rich holding extreme (life or death)
power over the poor. There is no middle class. 

As a hero. You are not a member of the upper class. 

##Classes
Careers come in three Main career categories.
Q: can some careers exist in multiple classes? it would provide a way for players to cross them.

Brawns - make their living with their bodies, speed strength, etc
Brains - Make their living through their mind.
Spirits - Make their living with their souls (magic)

i kinda want 5 but cant think of two others right now.

what about charisma? is that a whole class or just a type of whar FRP would have considered rogue. Classes should not be strict super classes but more attributes of careers. Each career fits into one or more classes.

## Brawns

Arena Fighter
Soldier
Construction Worker
Bouncer
Bodyguard
Hitman
Messenger (bike mssenger but new)
Deliveryperson
Larcenist
Office Worker - Since they are basically just bodies doing repetative tasks.
Factory Worker


## Brains
Decker
Accountant
Lawyer - Multiple flavors, Street lawyer, corp lawyer



## Spirits
Adept - Martial artist/monk
Technomancer - Since much of technology is broken but se are still familiar with it a class of mage developed around it.
Wizard - Practitioners of ”grotesque” (obvious) magic like fireballs and shit
Shaman
Street Shaman
Daemonist - Actually just someone skilled in the arts of portal manipulation and negotiation. 
Mechanic - More technically an archeologist. Someone well versed in the usage of extra dimensional technologies. 





# Skills

## Combat Skills
TODO: i gotta rethink a lot of the below since i came up with them thinking about a midevil 
fantasy set more than the futuristic i am going for.

Any character can wield any weapon and wear any armor (unless otherwise mentioned.) 
but without its specific skill penalties will be applied. These will be in affition to
any penalties normally imposed by the item itself (such as dex and spd penslties for heavy armor.)

Some items may require multiple skills. Likewise some skills apply to multiple types.
For example a two handed sword requires blades and two handed. A character with both 
who buys the Axes skill would gain the ability to wield two handed axes as well as one handed axes.

### Weapon Skills
#### Melee
* One Handed Blades - Any one handed sword
* Short - knives, daggers, stilletos, etc. gotta give some thought to if theres anything in
this category other than knife types. What about brass knuckles?
* Axes
* Two Handed
* Blunt - Maces and other one handed kudgles (except for “clubs” which can be wielded by anyone.)
* Flails
* Dual handed
* Staves - Staves do not also require “two handed”
* Pole Arms
* Whips - Includes chain weapons

### Ranged
* Thrown
* bows
* guns - includes crossbows
* Spears - spears javelins, etc

### Special
* Boomerangs
* Caltrops - Technically anyone can use caltrops. the skill is required to be able to
transport them on your person without periodically damaging yourself

### Siege
All siege weapons require their own skill, balista, catepult, trebuchet, etc.

## Armor
Armor types are cstegorized more by their firmness than weight. Weight has its own effect on encumbrance.
For example carbon fiber armor platemail would be heavy due to its rigidity even though it
has little effect upon encumbrance.

Light - Everyone can wear light or cloth armor without penalty. which is less useful than 
it sounds since most of it provides zero protection.
Medium - Flexible Armors such as Banded Mail, Chain.
Heavy - Rigid armor such as plate.
Shields - As in traditional handheld style hields. Personal defense fields cout as light 
for encumbrance rolls but act as heavy for defense rolls.