# Character Creation

# Step One - Choose A Concept

You are a practitioner of magic. the one technology unique to the human realm.

A practitioner is someone who burns Umbra in order to reshape the world around them. This could be by throwing
fireballs, travelling through distant computers to effect market trading, or literally reshaping the world.

# Common Patterns

  * Traditional "Merlin" type magicians. Their magic is time consuming and ceremonial with a heavy reliance on runes
  and "circles of power."
  * Martial Arts Adepts, "Technomancers" who have special effects upon technology
  * Daemonologists: Daemonologists are more like archiologists. They are magicians who are pretty much crap at all'
  types of magic except portal magic. They're really just excellent at linguistics and contract negotiation. 
  * "Shamen" and "Witchdoctors": Folks who use naturally derived techniques.
  * Superheros: Humans who's magic manifests as superpowers. These will probably be pretty common.
  * "Mechanics": These humans are not actually pale sensitive but have figured out ways to use alien weaponry. There
  has to be something special about them that makes it possible for them but not possible for other player characters.
  * Druids?
  * Witches: Heavily rely upon potions and charms. 
  * Elementalists: *cough* Avatar: The Last Airbender *cough*
  * Alchemists: Transmute things from one substance to another. Could be a bit like Full Metal to be more useful
  * Enchanters: Adept at stuffing pale into things. For the most part this item would then be bound to that enchanter.
  However the more advanced ones would be capable of creating things used by other people. Obviously highly adored 
  by mechanics.
  * Clerics: Acolytes of some deity.

# Background
Pale Umbra should encourage narative driven adventures over combat and grinding. Though players will undoubtedly want
to try out their fancy magics so there should be enough of that as well. Where did this character come from
Were they born as a slave or a free human? Are they from any of the myriad of mixed cities or a human only enclave.

What was the culture like where they are from. Were they raised by their own parents or in a more brood oriented
culture? Do they value strength, cunning or fielty? Are they frugal having grown up with nothing or extravigant because
the life of a gladiator offers great rewards but no guarantees.

# Character Traits
Since no story is interesting without conflict, characters should have personality traits, and/or history which could
potentially complicate an adventure.

These traits should not be known by other players (unless they are discovered naturally during role playing) but should
be given to the GM before the campaign is solidified. For smaller quirks these will simply provide an outlie which
the player can use to aid in a more fullfilling role-playing experience. More complex traits can be used, selectively,
by the GM to enrichen the adventure by incorporating them into the plot.


## Examples

### Simple Role Playing Traits
The character is someone who:
  * Is a perfectionist
  * Says “sorry” a lot
  * Doesn’t trust easily
  * Hates <race/organization/nation/religion>
  * Is just generally racist
  * Is deathly afraid of...
  * Is dishonest
  * Is abnormally selfish (Nearly everyone in Pale Umbra has to be at least a little selfish just to survive)
  * Has been dishonored but their culture forbids suicide so they are actively trying to die in battle.
  * Is a fanatic who will not aid others without it benefiting their organization/religion/family/whatever.

### More Complex Traits GM's May Wish To Leverage For Campaign Ideas
  * Has a strong personal or familial vendetta against...
  * Is on a personal quest to retrieve...
  * Is a deep cover a spy for...
  * Uses magic (probably not all just some of their most powerful spells) which require that they draw power from or 
  (even better) harm their allies. 
  * Is actually two characters. With the secondary being the juvenile charge the primary must protect. The charge could
  be the source of some of the players harder to use powers. 
  * Is being hunted by...
  * Periodically recieves orders from a voice they believe to be “God” Which they are compelled to obey (not the
  Christian god. They don’t even know about him.)
  * Is actually a parasitic species inside the player character. The parasite’s breeding period could occur during a
  campaign compelling them to try and infect another player.
  * Is two separate characters. One during the day and the other at night.
  * Has no idea what their powers are. Would require The GM to create the character and an adventuresome player
  Who would start the game with a nearly blank character sheet. The player would have to discover their powers during
  gameplay. Might be difficult to come up with a good reason for the other players to be bringing them along.

Some things which a GM could work Some or all of into a story to make it more interesting.

# Becoming a Practitioner
A character's spells and abilities are defined by purchasing them using Pale. 


# Complications
Prolly the 'more complex traits' above should be down here. Complications can offer more points to spend on creating
your character. There should be a minimum amount of complications required during character generation.

## Complications From Chumps
Complications in chumps are typically worth 5 - 15 points based on how much of an impact they will have. I.E. Rivalry
    is 5 points for an equivalent rival 10 points for a stronger rival and 15 points for a MUCH stronger rival.
They also typically have modifiers based on frequency that they would occur which range from 0 to 15 additional points
depending upon the complication.
And most of them have additional +/- modifiers for specific complications. Dependence, for example has a range based
on how much damage you would take, how long before the suffering effects and if it causes you to have to make a
skill roll to use your powers.

  * Accidental Change - If they have a secred ID they can accidentally change. Obviously useless here.
  * Dependence - Negative effects if they don't get the susbtance. This could be an addiction or something in their
  physiology.
  * Dependent NPC - 
  Distinctive Features - They can't have a disguise.
  * Enraged/Berserk - Certain conditions cause them to loose control of their toon.
  * Hunted - Hunted by a person or organization.
  * Negative Reputation - Causes others to react unfavorably.
  * Physical Complications - Character is physically restricted in some way. One eye, no hands.
  * Psychological Complications - Fear of heights, Code against killing
  * Social Complications - Restricts character from being able to interact socially (makes no sense in Pale Umbra)
  * Rivalry - Character has a rival
  * Susceptibility - Character takes damage from a substance or condition harmless to most people. Like Kryptonite or
  being on holy ground.
  * Vulnerability - Character takes more effect from a particular type of attack.
  * Unluck - Misfortune happens to character. No place in Pale Umbra, misfortune happens to everyone.
