# Combat
Combat is a fairly big part of most RPGs.

# Mechanics I am Pretty Solid On

## Actions and Turns
Combat takes place over some number of turns. Each turn the players and the various NPCs
get to perform some number of actions. Each turn is divided into 10 action segments
(or simply segments)

## Luck
The luck stat instead of being usable once per combat will be a burnable stat.
it defines a number of dice which you can add for just about anything but then theyre
gone until you can reclaim them.

See shadowruns edge for some ideas on how to reclaim luck dice p56

# Mechanics I am Less Solid On

## To Hit
Shadowrun seems to use a number of dice you need a success on (sucess being a five or a six)
the bit that IS good is fumbles. if half your dice are 1s you fumble. If half your dice
are 1s and none of upyour dice are hits its a critical fumble


## Out of combat mechanics
see Extended tests on Shadowrun p48 for an interesting idea.

TODO: Rename this file to something like game mechanics. Maybe a sub directory to sort things in.

# Unsorted Notes
Overall I want less math than chumps. It’s not about doing the math (the calculations are very simple) it’s about having
to remember all the varying formulas.

Chumps has this thing where if your body gets to zero you start to bleed. Then if you get to negative equal to your body
you die. That part I like. The fact that it's separate from 'stun' not so much.

How about that’s how hit points work but with stun. If you take as much damage as you have hit points (maybe called
vitality) you are stunned. If you take - equal to your hit points you die.

Maybe rules for your spells and running and shit degrading based on damage.

Maybe I also need to make it easier to recover hit points but only to a certain level?

Chumps “characteristic rolls” rolls against a stat to see if you accomplish a task, are 3d6 and you have to roll below
a target of 9+(stat/5) ie for an int 18 you need to roll below 13 (9+3). This seems overly complex but they must be
doing it for a reason.  Maybe so that they only have to use 3 d6? See if we can roll more directly against the stat
and do less math.
