# Crits and Fumbles
For entertainment purposes develop a wide range of crits and fumbles.

They would differ based on action but have common effects. Loss of next action, loss of a turn, failure of action, etc.

For example daemonist summoning fumbles could include a complete failure because it’s a national holiday in the other
dimension. summoning a less powerful being because their contracted being is ill. Accidentally violating their contract
with the being and thus being unable to summon them ever again (or at least until after a lengthy contract
renegotiation.)
