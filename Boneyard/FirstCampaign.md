# Some Adventure
Working on an adventure, which hopefully will become a campaign in Pale Umbra.


# Background

The party starts in a mixed Daemon region. It’s a little deep space no e except the humans aren’t the federation. The
daemon species that claimed that region still rules.

Do humans live on “reservations” in this region? Might be too cheesy.

They’re definitely second class citizens but not like “whites only” second class. There are laws that protect us and
grant us rights but there are no humans in the government.

But we arn’t the only ones. The daemons who rule this region are more trade oriented. Highly intelligent and shrewd.
They gain most of their land by purchasing it. In conflicts they rely heavily on hired mercenaries, and purchased
technologies. Because of their reliance on trade they are careful never to officially brand any potentially valuable
species as beneath them.

And they are more than happy to capitalize on human hatred for any species which these rulers want to take action
against. Especially unofficial action.

Most or all of the players are members of some type of guild (think shadowrunners guild) Which does “odd jobs” primarily
for the local government.

Each will have their own motivation for joining. Some are eager for payback. Others just in it for the money.

This guild is, of course, officially condemned by the ruling government and run by humans but there are rumors that it
has financial (and possibly other) backing from the local government.


# Plot
The leader of their local region (the entire 'nation'?) has taken ill. They have discovered that the illness is an
assasination attempt. The party has been hired to infiltrate a rival government and learn what they did to this leader
or retrieve something that would save his life.

The mission is doomed to failure for two reasons:
  1) First the party is actually being sent in as a distraction to allow a main force to actually perform the mission. 
  1) Second because one of the party members is a plant with orders to kill them all.

The assasination attempt is actually part of a plot inside the local government. The party is composed of known
troublemakers whom the conspiritor knows the local government would agree to loose. When the party engages with the
plant and before they kill him they will heard a large explosion and feel the ground shake. The plant has detenated
a device which just killed the main team.

At this point the GM will give the player playing the assasin their actual character sheet instead of the one
for the assasin which was created by the GM. Prolly want to trade character sheets With the other player as the conflict
begins to visually show the other players his demise is o.k. This will help throw the players expectations out the
window and hopefully establish distrust, remember the world of Pale Umbra is a dark and suspicous world without color or
hope. How many other players have secret agendas from the GM?

Question: How does this new character become a part of the team. Maybe he was their contact, a spy within the invaded
government leading them to their destination. He is now blown and wants to accompany them in their escape. If that's
the case he would have either been in on the assasination or lead them to the correct place. Perhaps they are the
equivalent of the 'troublemakers' the party is made up of but in the invaded government. They (and possibly a couple
of others) are being sacrificed by a conspirator in the invaded government. Perhaps he thought he was going to kill
the party and when they all find out they've all been sold out they must band together to escape. The local conspirator
has dropped them in a pit or locked them somewhere they cannot escape, not knowing the new party member knows the
way out because of some history.

The party has now escaped and learned of the conspiracy.

The leader, of course dies. The conspirator accuses the party of killing the retrieval team, preventing the retrieval
of the cure for the leader, and attempting to insight a war between the local government and the invaded government.
The military is sent to intercept them as they "attempt to escape." 

As a false attempt to defuse the situation the conspirator will also release the identities of the party members as well
as any known hang-outs outside of their own territory to the attacked government.

Not knowing of his co-conspirator's side plot and not having been informed of the escape (either because it's un-known
or because it is difficult for the conspirators to confer directly during this period of tension between these two
nations,) the local conspirator has not released any details about the new party member.

The new party member could take them somewhere safe where they learn that any of their own haunts have been raided
and friends and loved ones have been killed in an attempt to get to them. If they did not figure out the conspiracy
earlier they will definately learn of it here.

They now have the invaded government's forces hot on their tale. They are also hunted by their own government and must
infiltrate their own homeland in order to prove their innocence and expose the conspirator.

Question: Why do they care? They need a good reason to go back instead of just fleeing for some other part of the world.
They must have more family/friends back home. Perhaps the rest of their guild has been taken into custody.

Once they successfully invade their own home and begin to make progress a new complication will arise when they learn
that they are also being hunted by a powerful super-assasin (team or just mega boss?) from a third government. The one
with which the conspirator is conspiring.



...

Now what?
