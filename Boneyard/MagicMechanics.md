# Game Mechanics
Magic works by a player purchasing different kinds of powers which define very generically the effect they have but
nothing about how they appear. I.E. There is no “magic missle” spell but they can buy multiple levels of "Damage" And
define it as a magic missle or godzilla breath attack or anything.

After the main effect of the ”spell” is determined there are additional effects that can be applied. These work the
same way in that they define a generic effect in gsme mechanics and the player determines what they look like.
“Lingering,” for example, would be used to add an effect that causes additional damage on subsequent turns. This could
be described as the attack poisons the target or that it sets them on fire. Since we are describing magic, it can be
assumed that these effects cannot be negated by traditional means. Fire for example would be a magical fire which cannot
be extinguished by water. Players, however, are encouraged to use their imaginations. Perhaps it CAN be extinguished
by stepping into a vacuum.

Lastly spell effect choices are not only limited to good things. Negatives can be applied to an effect in order to
reduce spell costs. Perhaps a playes character has the ability to teleport but doing so causes them to take massive
damage. Making the spell useless in general gameplay but very useful when backed into a corner.

## Spell Mechanics/Spell Caveats
  * Spells are assumed to be able to be cast at range. TODO: Come up with the default range, 200m or something
  * Spells are assumed to be instant effect unless modified.
  * Spells are single target unless modified. Gonna need some extra detail for things like desolidification.
  * Spells take effect for one turn unless modified. With exceptions for a few like drain. This is one full turn so
  entangling someone or lifting someone they stay lifted until that same action phase on the next turn.
  * Spells require sight but not neccessarily "line of sight." You have to see your target but...
  * Spells require a clear path to the target but not neccessarily a straight line. I.E. Fireballs can be thrown around
  things if you can see the target.
  * Spells do not require any specific incantations or gestures (words or movements) to be cast.
  * Spells can be percieved. They are assumed to be perceptable by at least 2 of the 5 normal senses shared by most
  races (sight, sound, smell, taste, touch.) Typically these will be sight and sound. However any two can be chosen
  but they will be perceptable to these senses at range. I.E. if your barrier is invisible but can be tasted everyone
  nearby will notice a strange taste in their mouth without requiring they lick the barrier itself. In addition Pale
  sensitives can tell when a spell is being cast.


# Range
All spells require that you be able to see your target. In addition to that they have a maximum range of 10 meters *
the spells base cost.

# Umbral Cost
A practitioner must burn Umbra (the accessible form of the Pale stored in their bodies) in order to perform magic. A
spell's cost is deducted from their Well every phase the spell is active. I.E. For a fireball spell which takes effect
immediately on the phase it is spent the Umbral cost is detucted when it is cast. A barrier spell costs Umbra from the
moment the barrier is created until it is dispersed (willingly or not.)

A spell's Umbral cost is 1 Umbra for every 10 active points worth of spell.

A spell can be used at a lower effectiveness. I.E. If you've purchased 6 dice of Damage for your magic harpoon, you can
choose throw a 3 die harpoon. But you must announce it when you announce you're casting the spell and before you roll
any dice.

# Spell Effects

  * Damage - Causes physical damage.
    * Cost: 5/die
  * Heal - The opposite of the above.
    * Cost: 10/die
    * Note: Likely will need to define a daily maximum.
  * Resist - Resistance to damage.
    * Cost: 15 - 25% reduction, 30 - 50% reduction 60 - 75% reduction.
    * When purchasing resistance the type of resistance must be defined.
      1) Physical Attacks
      1) Energy Attacks
      1) Attacks with a specific effect (magic, ice/cold, weapons from a specific race) drop the cost to 10, 20, and 30.
  * Suppress - Reduces a target stat or spell temporarily (Chosen at purchase.) Requires a normal attack role.
    * Cost: 10/die
    * Examples: Can drain the power from nearby electronic devices. Can make a target physically weaker.
  * Enhance - Increases a target stat or spell temporarily (Chosen at purchase.) Requires a normal attack role.
    * Cost: 6/die
    * Examples: Boosts the power of any fire related spells. Makes the caster as strong as 10 men.
  * Portal - Creates an intradimensional portal.
    * Cost: 1/1 meter of distance.
    * Notes: Portal is fairly cheap because it is still not terribly useful without modifiers.
    Portal, however, is an exception to the "Clear Path" requirement. I.E. you can portal into or out of a box.
    Though you still need to be able to see the target, it can be used to pass through standard barriers.
  * Physical Manipulation - Telekenisis, grasping tenticles sprouting from the ground, etc.
    * Cost: 3 points/ 2 points stength
  * Barrier - Magical walls, etc.
    * Cost: 3 Pale for a 1m long, 1m tall 1/2m thick barrier w/ zero health. +1 point per m length or height, 1/2m
    of thickness or point of health.

  Kinda want to see how the above works out before defining these.

  * Manipulate Matter - convert lead into gold, dirt into food, etc. This also includes changing the shape of things.
  However you have to define what it does and it can only do one thing. I.E. you can convert lead into gold. OR you can
  change the snape of a thing into a non-complex machine. So any material into a single material you define ahead of time.
  Or any material into any simple shape but still the same material. Cannot be used on living things. This might still be
  stupid.
  * Desolidification - Pass through walls, etc.
  * Summon - Ability to sommon things from somwhere else. Has to be something you have strong familiarity with. Like an
  object you are touched or another Daemon you have a contract with. Does this work on things like a key from across the
  room which you have never touched. Summon is related to portal. Essentially you compel something to form a portal and
  come to you.
  * Size manipulation - Growth, shrinking. Have to buy one or the other separately. This can also effect total
  mass/density but once again separate purchase.
  * Clairsentience - A given sense (sight, hearing) at range.
  * Sense Manipulation - Illusions, invisibility, etc. The base spell is only active while it is being cast, blindness
  and deafness would be Sense Manipulation plus lingering.
  * Reflection - The ability to send a spell's effects back to it's original caster.

## Powers Which should be included but I have to figure out

  * Shapeshifting - Maybe into something w/ the same mass. Maybe into another living thing w/ roughly the same water
  content

### Notes on Specific Spells
The above is intended to be very generic and give you abilities limited only by your imagination.
Some examples of specific "spells" which can be built by the above.

  * Flight - Physical manipulation with only self.
  * A vampiric touch - Drain(with the disadvantage of requires contact) Plus recovery (with the disadvantage of only
  self) and the disadvantage of linked
  * Wall walking - Flight (or physical manipulation) with the disadvantages of only self and requires contact with some
  surface
  * Blind - Sense Manipulation with Lingering
  * Absorbtion - Resistance + Recovery Linked

## Powers which may or may not be included

  * Tunnelling? - Is this just portal with persistence or portal with damage? Plus some disadvantage like "local" or
  something. Also requires contact.

  * Regeneration - Like Recovery but passive and always on. Works when you are asleep, unconcious. Happens slowly
  over time. Like recovery gets puchased for a specific stat.

  * Making plants grow - How would this work? Sounds like a definition of matter manipulation but if I say that can
  effect living things I need to come up with another way to say this cannot be used to turn a living heart into a chew
  toy.

TODO: Do we want a stun mechanic
TODO: Do we want a knockback mechanic?

Prolly more

# Spell Modifiers
Additional effects can be applied to spells which either give advantage or disadvantage.

These modifiers will increase or decrease the Pale cost of the spell as well as effect it's Umbral cost
Formula from chumps

Active Cost = Base cost * (1 + total value of all advantages)

Costs defined below are a multiplier on the base cost. For modifiers with multiple cost options bulleted lists mean
choose from the available options and add the costs together. Numbered lists mean the cost is tiered and each tier
includes the previous tiers.

## Advantages
Advantages are modifiers which make a spell power do more stuff and make it cost more.


  * Area Effect - By default spells have a single target. AOE adds a defined size.
    * Cost Modifier: +1/4 per each increment up to; 4m radius, 8m cone length (or slant,) 16m line
  * Multiple Targets
    * Cost Modifier: +1/2 per each additional target
  * Piercing - Reduces resistance rolls
  * Contingency (mine) - Ability to cast a spell which does not take effect until something triggers it later. The
  trigger must be defined ahead of time (touch, proximity, remote detonation by player, etc.) Levels:
    * Cost Modifier:
      * +1/4 Per simple condition or (closely) related set of conditions. I.E. when touched, when subject to vibration
      (including sound)
      * +1/2 Per complex condition. I.E. Contact with a barrier at speed. Touched by a living being.
      * +1/2 If the condition can be defined at casting instead of purchase.
    * Notes: If multiple conditions (simple or complex) are defined the player must define if they are anded, or ored
    together.
  * Immunity - Spell cannot effect the good guys. Useful for AOE attacks and to prevent reflection.
    * Cost Modifier:
      1) +1/4 - Self
      1) +1/2 - Party
      1) +1 - Definable when using the spell.
  * Cumulative - Can be applied multiple times for an added effect
    * Cost Modifier: +1/2
  * Increased Range - Can be cast further than the normal range
    * Cost Modifier: +1/2 per double range
  * Undetectable - The spell cannot be perceived by normal means.
    * Cost Modifier:
      * +1/4 cannot be perceived by a single sense
      * +1/2 cannot be perceived by all normal senses
      * +1/2 cannot be perceived by Pale sensitives
    * Notes: This includes characters defined as non-magical. If you want a weapon to be really a weapon and not
    something that the practitioner is unknowingly casting spells through you must purchase the pale sensitive modifier.
  * Lingering - Effects stay and dissapate over time. Setting things on fire, poisoning.
    See chumps pp 103 for some ideas.

### Advantages I'm not so sure about
  * Does not require line of sight - Maybe this is not an advantage but just the spell plus clairsentience linked.
  * Through Time - Not sure how to work this in but some effects, specifically clairsentience. There will have to be
  some specific modifiers like this cannot be applied to portal.

## Disadvantages
Disadvantages are modifiers which make spells harder to use and cause them to cost less.

  * Concentratioon - You have to focus the entire time the spell is being cast. Cannot be applied to instant spells.
    * Cost Modifier:
      1) -1/4 character's DS is reduced by 1/2 while the spell remains in effect.
      1) -1/2 Character's DS is reduced to zero while the spell remains in effect.
  * Reduced Range
    * Cost Modifier:
      1) -1/4 Requires physical contact
      1) -1/2 Only works on self
  * Linked - Two or more spells are cast at the same time and cannot be cast separately. The disadvantages cost modifier
  is applied to the lesser (lower active point cost) spell. If two spells have the same cost choose one.
    * Cost modifier: -1/2
  * Requires Focus - Requires some physical object which could be taken away, lost or targeted in combat. A magic wand,
  a ring, etc.
    * Cost Modifier: Cost modification varies based on how obvious and accessable (how easily it can be taken a way.)
      1) -1/4 Inobvious, Inaccessable. I.E. a ring or a pendant. Something that could easily be covered up and would be
      difficult to remove from the character while they are concous.
      1) -1/2 Inobvious, Accessable. I got no example here
      1) -1/2 Obvious, Inaccessable - A Ring which glows when the spell is cast. A launcher attached to the forarm.
      1) -1 Obvious, Accessable. A wand or staff.
  * Preparation - The caster must do a thing ahead of time to be able to cast it. Preparation _cannot_ be done during
  combat.
    * Cost Modifier:
      1) -1/4 The spell takes effect on the next segment
      1) -1 The spell takes effect on the next turn (same segment)
      1) -2 The spell must be cast at least 10 minutes ahead of time.
  * Incantations or Gestures - Requires specific words or movement to cast. So if you are mute you cannot cast or if
  your arm is broken you cannot cast it. The incantations & gestures have to be specified ahead of time.
    * Cost Modifier:
      1) -1/4 for 1 hand or voice
      1) -1/2 for both hands or voice and one hand
      1) -3/4 for both hands and voice.
  * No Witnesses - This spell cannot be cast if anyone (NPC or PC) is witnessing it.
    * Cost Modifier: -1
    * Notes: Remember spells are percieved by at least two normal senses and by Pale sensitives. Unless
    undetectable is purchased for all normal senses as well as pale sensitives simply having other party members
    turn their backs will not be enough.

  * Stupid - Certain power combinations just do not make sense. Stupid is a catch-all disadvantage for all of those.
  Stupid will not have a specific cost modifier but rather be at the discretion of the GM. Examples include:
  clairsentience for taste, The ability to taste at range. Invisibility + No Witnesses. Stupid spells should be
  nearly free and useless for much more than adding depth to the story.

  * Consumes - Ammunition, spell components, etc. It is assumed that the practitioner is able to fill this up at the
  beginning of the campaign. How hard the material is to acquire during a campaign will take some more thought.
    I will define this one later.

### Other Disadvantages
Disadvantages I'm not so sure about

  * Damage Self - Using this spell causes you physical damage. Definately want to add this one but have to figure
  out how it will work.
  * Straight line - Might need some more thought but the spell only works in a straight line.
  * Physical manifestation - Gotta give some thought to this but there has been some permanent change to the
  practitioners body that is inconvenient which is required for them to use the spell. I.E. They now have wings to be
  able to fly or spines coming out of their arms for some mele damage. Maybe this is just a focus you cannot hide or
  lose? If the manifestation is temporary like wolverine claws it does not save you any.
  * Slow effects - A spell takes effect over some number of turns. I.E. healing that happens over 6 actions. You do not
  have to concentrate while this is hapenning. Is this redundant with Sustain? It does not make its effects overall
  stronger like sustain

# Frameworks
I think it is too early for frameworks but once I get the rest worked out I will want to include them.

A framework is a way to get more bang for your buck. Spending Pale on a framework gives you the ability to buy a number
of different spells all for the same cost. However spells purchased as part of a framework are mutually exclusive. Only
one of them can be cast at a time.

## Why are the Spells Tied Together
Just like a spells appearance needs to be defined a framework's reason needs to be defined. Why are you able to share
these spells together. They can't just be lumped together for the points cost.

Perhaps they are tied together because the practitioner is an elementalist able to shoot throw ice darts or form a
barrier of ice but not both at the same time (though the ice wall will probably stick around for a bit.)

Or perhaps a technomancer with reconfigurable gadgets, quickly dismantle a laser rifle and convert it to a long range
listening device. Flip a few components over and add some more to open a portal to escape certain death.

Or a daemonologist who has negotiated contracts with beings from multiple races. Beings who might not get along with one
another.


# Character Creation
As a thought on how many points to give players to create their toons.
300 Pale + 50 or 100 Pale worth of complications
