# Player Characters

Pale Umbra is a narrative driven world. These naratives in turn should be  
driven by the characters in them. Therefor it is important to put some thought  
into your character. Their goals and desires, Their fears and hatreds.

A character's backstory could provide a plethora of material which the  
GM could use to add layers to the plot of an adventure.


# Character Creation
Character creation for Pale Umbra will be a slight modification of the rules  
found in [Champions Complete](https://www.amazon.com/Champions-Complete-Derek-Hiemforth/dp/158366145X/ref=sr_1_1?dchild=1&keywords=champions+complete&qid=1634073228&sr=8-1)

## Overview
Pale Umbra and the Hero System emphasize creative freedom. Spells and abilities  
(powers in the Hero System) abilities are purchased (using Character Points  
allocated by the GM) from a relatively concise list of generic effects and  
the actual behavior and appearance of the Spell or ability defined by the  
player. This means instead of a typical system where you would buy a specific  
power such as "fireball." You buy some amount of a "Power" called "Blast"  
which is simply damage done at range. Then you define, at character creation  
time, that your blast is a fireball or a lightning bolt. It could be a machine  
gun or quills thrown off the PC's back.

Powers are also able to be modified. If you want your fireball to explode  
you modify Blast with "Area of Effect." Which makes it cost more. Likewise  
if your character is using a weapon which shoots fireballs (and thus could  
be taken away from them.) You modify your fireball with "Requires Focus"  
causing it to cost less to purchase.



### Modifications

## AnyMan Skills
In Hero System new characters get a number of skills for free. For Pale Umbra  
we modify that list in the following ways

### Skills Removed
"Transport Familiarity" - Humans are unlikely to have ever driven a car.

### Skills Added
Disguise - Only usable on non-humans. Due to the differences in our physiologies  
they have difficulty telling us apart. Even humanoid species will have this  
limitation the same way we have difficulty telling Gorillas apart.

