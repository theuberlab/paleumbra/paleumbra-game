# Daemons
Daemon, also Demon or Deamon and all pronounced exactly the same. Is the term applied to any species which has
achieved its [third age](TheAgesOfMan.md) by discovering inter-dimensional travel.

Those who engage in trade or war with other dimensions are commonly referred to as the Known Daemons.

#
