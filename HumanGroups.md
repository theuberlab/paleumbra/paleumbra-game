# Human Groups
Human "Nations," organizations, religions, etc.

# Countries
Nations with land and organization.

## UCAN - The U.S. Canadian Aliance
During the mass influx of Daemon races, and before the discovery of magic the remnants of the U.S. and Canadian
governments bound together into a single 'temporary' aliance. As the wars raged on the aliance morphed into a
de-facto government confined to some segment of the western portion of the North American continent.

## CSA The Confederate States of America
Yes those guys. They have 're-formed the confederacy as it should have been' in the southern US. They have no issues
with any other humans, black, latino, asian, whatever. They hate all Daemons and will kill anyone they thing is 'mixed'
without hesitation. Of course the people they kill aren't actually mixed.

The CSA's position on magic is mixed. Officially they aren't against it though many high officials are. Within the CSA
it is best not to practice openly.

# Clandestine Organizations
Groups with no borders who are, or who began as, resistance organizations. 

## IFRC - International Federation of Red Cross and Red Crescent Societies
The largest humanitarian aid organization on earth. With a great deal of experience working across borders and cultures
they transformed from an aid organization to a resistance organization. Though not entirely. They focus on training
local groups how to establish their own local resistances.

## The Legion
[The French Foreign Legion](https://en.wikipedia.org/wiki/French_Foreign_Legion) is an organization within the French
army who's membership is open to foreign recruits. They are highly trained and known for their strong esprit de corps.
Members of the Foreign Legion do not swear allegiance to France, but to the Foreign Legion itself.

When one of the less agressive races entered earth and 'bought' (more like made them an offer the couldn't refuse) the
land which is currently known as French Guiana under conditions that would result in the enslavement of the local
populus the legion resisted. As word of their heroism spread former legionnaires 'reactivated' themselves creating
cells all over the world and becoming one of the largest human military organizations still alive.

Having absorbed numerous smaller military organizations and roots in splitting from France The Legion no longer
considers itself, or is considered by others, as French. They are dedicated to the freeing of human slaves.


## Anonymous
When the worlds networks were destroyed those in IT found themselves in a world for which they had no skills.
When magic began to make it's way into the world there were those who saw the opportunity to replace the technologies
which they had lost.

Anonymous is a small organization made up entirely of technomancers. Including some of the most powerful of them.
Through their mastery of tech they are able to communicate with one another instantaneously through the Pale no matter
where they are on earth. 

By and large they are dedicated to disrupting those daemons they deem 'hostile to the human endevor.' However
like the modern Anonymous they lack a formal leadership structure and include anarchists and paranoids who simply
revel in distruction as well as egomaniacs who simply wish to flex their skills.


## The Unhinged
Less of an organization and closer to a plague. When the old one was killed in North America a large number of humans,
particularly those with certain types of mental issues (need to get specific) began moving towards it. When they 
ecountered resistance, such as medical aids in hospitals trying to stop them from leaving, they became absurdly violent.

It is thought that the old one reached out to something in them and called them to it for protection. 

Since it's death even the most docile became increasingly more violent. The cry of the old one also gave them heightened
strength and reflexes. They now inhabit the area around the Laurentian sea killing any who enter human or Daemon.

Since so much time has passed since the original incident it is not well understood how the Unhinged are still around.

The two most popular theories are that The Old One's cry also extended their lifespan or that some Unhinged are able to
perform a similar cry and thus 'recruit' more humans to their side. Neither theory has been able to be substantiated
by research.



## The Children of Víðarr (Romanized as Vidar or Vitharr)
A group made up primarily of those of Skandinavian descent who blame Daemons for the destruction of Iceland.
They named themselves after [Víðarr the Norse god of vengance](https://en.wikipedia.org/wiki/V%C3%AD%C3%B0arr) and
considered The Great Daemonic Wars to be Ragnarok. Their members, who call themselves [Seiðr](https://en.wikipedia.org/wiki/Sei%C3%B0r)
primarily practice Norse Runic and glyph magic though since many of their members are those of scandinavian descent
from all over the world any form of magic can be found amongst them.


# Paramilitary Organizations
Real paramilitary organizations who might be a good basis for clandestine organizations.

https://en.wikipedia.org/wiki/List_of_militia_organizations_in_the_United_States
According to wikipedia these three active paramilitary organizations here in the US have a national presence.

  * [The Constitutional Sheriffs](https://en.wikipedia.org/wiki/Constitutional_Sheriffs_and_Peace_Officers_Association)
  * [Oath Keepers](https://en.wikipedia.org/wiki/Oath_Keepers) - Mostly an organization of ex military, police and first
  responders who have taken an oath to 'defend the constitution'
  * [Three Percenters](https://en.wikipedia.org/wiki/Three_Percenters) - Strongly anti-government.
  
Some others which look interesting

  * [The Michigan Militia Corps](https://en.wikipedia.org/wiki/Michigan_Militia)
  

# Criminal Organizations
Real criminal organizations alive today.
Criminal organizations would already have good organization and skills at distributed leadership and staying undetected.

  * [The Triads](https://en.wikipedia.org/wiki/Triad_(organized_crime) - "The" Triads are not all together. There are
  a number of them from various S.E. Asian countries.
  * [MS-13](https://en.wikipedia.org/wiki/MS-13) - A gigantic gang from El Salvador.
  * [Yakuza](https://en.wikipedia.org/wiki/Yakuza)
  * [Ndrangheta](https://en.wikipedia.org/wiki/%27Ndrangheta) - Northern Italian, According to Guardia Di Financia one 
  of the largest and best funded criminal organizations in the world.
  * [The Sinaloa Cartel](https://en.wikipedia.org/wiki/Sinaloa_Cartel) - The folks running Mexico.
  * [La Cosa Nostra](https://en.wikipedia.org/wiki/Sicilian_Mafia) - The Scicilian Mob
  * [Bratva](https://en.wikipedia.org/wiki/Russian_mafia) - The russian mob.


