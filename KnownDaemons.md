# Known Daemons

Races which will be defined by the game

## Name Ideas
  * Subjat (pronounced as a y)
  * Djuwelli
  * The El. El being the general term for dirty in Old Semitic languages (Which Arabic is descended from)
  https://www.britannica.com/topic/El
  * Something derived from Ishtar, (Akkadian)/ Inanna, (Sumerian ) - The ancient Mesopotamian goddess of war and sexual 
  love.
  * Qhirod 
  * Nurqin
  * Ydeel 
  * Ocid
  * 
  * Eerkae 

# The Species

## Species Template
Use this as a template to describe a species.

### Description
A summary of their culture and history.

### Physical Appearance
What it says on the tin.

### Habitat
What does their homeworld look like? How much do they try and force their locality to conform to their home preferences?

### Warfare
What types of weaponry do they employ.

### Allies and Enemies
Which races they ally with, wich races they fight. Include dispositions with many of the other races.

### Humans
Obviously we start with them. _probably_ the only playable race.

### History/Discovery
Some information about when they joined the known daemons, when they discovered earth, etc.

### Release
When should this species be discovered in the game? Do we know about them at the start or will we learn about them later?


## Girik

### Description
The Girik are medium sized (1-2 meters) oblong floating gasbags.
They come from a world of high humidity and low mineral resources.
Being largely unsuited to combat they are frequently traders, bankers and
finaciers.

They understand science and technology well though due to their homeworld's  
limited resources they are less adept at material sciences (metalurgy, chemistry, etc.)

### Physical Appearance
Girik have binoptic and binaural sight and vision like humans however their  
sense organs are turned 90º (on top of one another instead of side to side.)

They are able to produce various gasses (given a proper diet) through glands  
which excrete into internal poutches enabling them to ascend and descend.

They also posess a small pair of leathery bat-like wings which while too  
small to provide any substantial lift allows them to navigate horizontally  
as well as giving them greater control over their vertical travel.

They have 2 long skinny limbs which end in long sharp claws. These are  
typically employed to gather food from the walls of their habitat but are  
also quite deadly to other Girik.

Girik skin is surprisingly thick for a balloon-like species. So while punctures  
are incredibly high risk for them they are somewhat difficult to puncture.

Internally the Girik are filled with multiple spaces giving them additional  
protection against more minor punctures.

Girik have wide mouths which only open about 12-15º. Just enough to allow
their long wide tongues to loll out and wrap around food.

### Habitat
The Girik come from a world which is covered in exceptionally deep narrow chasms.

One result of this is that Girik are, almost without exception, extremely  
agoraphobic. On earth they tend to build cities with buildings that are at  
most 2 meters from one another. There will be no roads (having no use for cars)  
only walkways. Also having no need to touch the ground they will tend to ignore  
the ground. They will keep it tidy but spend little to no effort to ensure  
walkways are smooth except in important/highly trafficed areas.

The Girik homeworld is highly humid resulting in a wide variety of mosses  
and molds which are the Girik's natural food source.

They do not terraform when they live on earth however Girik cities will
be built in highly humid areas and frequently implement misters to suppliment
humidity on dryer days.

Girik buildings are not only tall and close together but painted with a  
thick porous material similar to adobe. This provides purchase for the mosses  
and molds upon which they subsist.

### Warfare
The Girik prefer to avoid warfare if possible.

### Allies and Enemies
Which races they ally with, wich races they fight. Include dispositions with many of the other races.

### Humans
The Girik get along well with humans. Allowing them to live in Human cities.

This was a more recent change. Originally the Girik were entirely ambivilous  
to humans finding that we had nothing to offer the Girik. Since the discovery  
of Magic the Girik have become more welcoming. Wither this is out of fear  
or because they feel they can gain something from us is unknown.

### History/Discovery
Some information about when they joined the known daemons, when they discovered earth, etc.

### Release
V0 - The Girik will be one of the first species encountered.



## Dhumean
The Dhumean are a-holes. They have achieved most of their success through  
military expansion. They are obsessed with Magic and want it for themselves.  
They are well known to be performing experiments on humans attempting to  
extract Magic from. They typically breed their specimins in captivity but  
raids on human settlements to "refresh their stock" are not unheard of.

### Description
A summary of their culture and history.

### Physical Appearance
What it says on the tin.

### Habitat
What does their homeworld look like? How much do they try and force their locality to conform to their home preferences?

### Warfare
What types of weaponry do they employ.

### Allies and Enemies
Which races they ally with, wich races they fight. Include dispositions with many of the other races.

### Humans
Obviously we start with them. _probably_ the only playable race.

### History/Discovery
Some information about when they joined the known daemons, when they discovered earth, etc.

### Release
V0 - The Dhumean will be one of the first species encountered.



## Species
One of the races which actually interacts with and are willing to help humans is a warrior race who believe so 
strongly in dominance by force that their definition of sentience requires you be able to kill them. Human beings
were considered livestock until they developed magic and thus a fighting chance. 

In their culture it is customary to mount the skulls of one’s ancestors who have died in battle and hang them on the wall.
This is so important to them that they have a small ceremonial way for announcing that you are approaching to drop off
the bodies of those you have defeated to your enemies. This ceremony is inviolate and one of the main reasons they take
part in the war is that one of the other races tricked them by using the process in the past.

Some of the other races use disintegration weapons. Since this leaves no bodies and this race takes great offense.
Perhaps they were even eating humans but once they learned of our sentience they stopped and they released all of us.
They didn’t give back any occupied land though we have to take that by force.

Possibly the main reason that they don’t hate us is that the first times we killed some we left the bodies where they lie.

Do NOT kill with disintegration in front of them. It is very likely to cause a loss of trust.
They have no problems with ranged weapons or even poisons but they don’t use explosives.

For the warlike species. If being able to kill them is part of a criteria for sentience they probably don’t have any
predictors in their world. How do they feel about disease. Being able to kill them isn’t the only criteria so they
don't assume it’s sentient.

## Species
Another race is a large ugly multi legged species. They are covered in bumps from which poke eyes and small vestigial
limbs. We only ever see their females. Actually we just think that is the case. The reality is we only ever see their
males after they have become absorbed into the bodies of the females. The males are much smaller, weaker and less
intelligent. They aren’t smart enough to travel to our dimension.  When a female enters heat she travels back home
where a male will seek her out and latch on to her side where it will slowly become absorbed. She will then lay a
batch of eggs.

## Species
One species the females are so much less intelligent than the males they are like animals. They don’t keep them in kennels but breeding involves the males chasing them down. If a male can catch her she may become arroused. Then she will take the aggressive role.
All of their race are born out in the wilds. A mother stays with her young for only a few weeks. Basically like a foal once it learns to gallop around it can hunt. Then the mother will wander off.
They are obviously a solitary species. They love nature but do build cities of a sort. Large sprawling things which bridge the giant trees on their home world and you might go for days without seeing another person.
They are a very fast race. They have three sets of limbs. The back two pair are primarily used for locamotion but also for grasping and manipulating. The fore pair are the most dexterous and usually used as “hands” but for real speed they will drop to all six. They are also excellent climbers.
If players meet one of these he may tell a wistful story about his last compilation.  Of the sweat rolling down his back as he kept and ran to chase her. Then if her strength when she turned and caught him.  He will happily show the scars from where one of the bones in his arm broke the flesh when she snapped it slamming him against a rock.


## Species
A species of warlike plants. On their native soil they moved slowly. Basically
Uprooting and replanting themselves with long tendrils. Kind of like sentient crab grass.
It’s a very slow process. Until their dimension was invaded and they realized they needed
to move much, much faster to survive. So they built mechanical flowerpots with tank
treads (fancy tank treads which can handle stairs) and weaponized torsos
(they’re a little dalek.) Most species don’t even know they are plants. Perhaps
that fact doesnt go into the first edition.


TODO: There are two Matemá sections. merge them.
## The Acolytes of the Great Matemá
A robotic “species” that take a form that look like monks. In reality a single computer. A technological singularity
in its dimension. 
https://en.m.wikipedia.org/wiki/Technological_singularity


It is a single being which sends out remote drones to do its bidding. This might be one of the primary adversaries.
It’s here for electronics components. It’s taken residence in DRC. It is the only “being” to be able span multiple
demensions (or for that matter even a portal in a single dimension.)
Because it really is just multiple computer arrays in each one which communicate wirelessly through the Pale.

If they are disconnected offshoots the drones are still more intelligent than humans. Matema functions like a compute
cluster.
 
Moves in to ours to mine for components used in electronics. Calls itself the acolytes because it both intimidates
other species as well as causes them to underestimate it in no small part by hiding the fact that they are a singularity.


## Species
A species which has three genders
Perhaps it’s actually a symbiotic thing. The two “males” deposit sperm into the female where they combine and she
gestates the brood. Perhaps the “female” is actually a different species and for hundreds of thousands of years what’s
been happening is that they have been allowing the other species to plant their eggs in them because they actually just
eat thirty or fourty of them and then “give birth” to about a dozen.

## Species
A symbiotic species which has very little central structure. Massive forelimbs, a weak spine and a massive amount of
tentacles. It would embrace another being like. Human. Their arms attaching to the backs of the humans arms their
tentacles would wrap around the humans legs and torso.

It would inject needles into the humans body and spine. It would drain nutrients fron the humans body and in return
excrete a substance which makes the humans bones and muscles grow stronger.


## Species
Maybe one comes from a dimension where the entire thing is one enormous chasm so their eyes are only up and down.
this sounds kinda dumb

## Species
A gasbag one who floats like a zepplin. Maybe thier eyes are not on a plane like owl ears.


## Species
One like a centaur but from a bug. With a head that doesn’t swivel at all because it’s got multi segment eyes and it
just rotates it’s body instead of turning it’s head. It would have four relatively small arms on the front of its body.
They can’t reach out to the sides like ours or even up and down very well. but it’s torso is so flexible it doesn’t matter.


## Species
One that walks on all fours and has enormously dexterous lips and maybe multiple tongues.


## Species
One that is a little like the puppet masters from the Heinlein book but nice. They do the whole Dax thing but one of their
criteria (at least lately) is that the host actually not want to be a host at some point. They do the integration only
late in life because they want to ensure that the personality of the host has been strongly enough developed so that it
is not subsumed by the creature.

There will be one that was implanted too early. Or it’s host wanted SOO much to be a host that it played them and
pretended disinterest for a while. This one would have a marked disregard for how precious life is because it’s been
alive for so long. Which is why thy want to make sure the host personality is strong. Maybe this way the host
personality remains more intact and they can “converse” or maybe it’s just that they don’t want to forget how to care.
Perhaps they have two factions. Because the one above has no respect for other life it became a concourer.
Implanting others of its species in very young members of other species and creating others like itslef.

The main body of the race fights to stop him. Maybe they are so peaceful that they are having trouble.
They are affraid to openly attack the others because it will get their hosts killed.
Obviously they need to hook up with the warlike species but since these cant kill
that species that species wont even talk to them.

Maybe theres one guy or a small troup who have? Maybe its just a good idea for a PC.


## Species
A fungal like species. Like a sack fungus. The areas they take over will just be filled with spores. Their species
have both genders. They go through cycles where they spew spores(I think I need a different word) which land places
and on other cycles they spew pollen(again unsure of the word) which causes fertilization.

So they of course land anywhere incliding on other living species. They are surprisingly wiling to tear down spores
attaches to their own homes. Evolutionarily this is because plants that grow too close starve each other for resources.
They will probably feel that protection which all humans feel for all babies once a sproutling (need a fungal name for
this) reaches a certain developmental stage. Before that they feel basically the same way that humans feel about sperm.


## Species
Definitely at least one non-insect eusocial (hive like) species. There are two eusocial mammal species on our planet (both types of mole rat)

They would have developed into a situation where each city is essentially a hive (let’s call them nests.) so there might be a Denver nest and an LA nest.

They probably get offended if you call them hives.

They would probably be a confederacy. No queen is going to want to be second queen. It works out because they all have the best interests of the species as their top priority. And queens want their own territory and have no desire or even ability to rule over a different nest.

That’s not entirely true there are some species that have multiple queens. Maybe a major queen for the earth and a minor queen per city. The minor queens actually run things because all the major queen does is give birth.

Can they hijack some eggs?



## Species
Prolly up where the fallout from pounding the birds old one w/ 3 nukes would have gone. Much of it would have absorbed by the creature itself but there’s prolly a highly toxic area. There will be at least one race that likes it there. Prolly the north of Britain or maybe they’re building their own island. They’re one of the races sympathetic to humans. They got there early and started mining the oil in that region. Then some stronger species kicked them out for the resources but found that neither they nor the human slaves they took could mine it. So they begrudgingly agreed to let the first race come back, mine it and sell it to them.

Ironically the other race no longer keeps human slaves but that’s just because we‘re weak and not worth the upkeep. We _can_ trade with them but it’s sketchy. We’re like annoying little yappy dogs and they will kill us at a moments notice.


Most of Brasil is home to one of the worst species. There are few of them but they
are massively powerful. They work humans to death. They have captive breeding programs
but since we take so long to be useful the often raid other nations and steal their slaves.
Which is a big part of why they are at war with a couple of other nations.


Some nation didn’t arrive as concourers at all but as merchants. They bought the land they
occupy (from both humans and Daemons.) humans go there willingly because it’s safer.
However if you have been branded by one of the other races as one of their slaves this
race will cage you up and send you over. It’s part of how they manage to remain neutral.


# Visual References For Inspiration
Some of these have interesting looks about them.

https://images.app.goo.gl/PqrsMX5eNsrUBrRQ9
https://images.app.goo.gl/TzuaJjJbQ6a4x6Ur5
https://images.app.goo.gl/Vuw8mmG7JAcooBjP8
https://images.app.goo.gl/AoZVdV37AgBy69HW9
https://images.app.goo.gl/XgXGBvsSy5WWwtLt7
https://images.app.goo.gl/RuDdP7meR1ch7VSx9
https://images.app.goo.gl/Y5Urj8C9QBKa78Gh9
https://images.app.goo.gl/iyRB6tAN5fvBuqHg6
https://images.app.goo.gl/aT4F5fhKga6Vm7B7A
https://images.app.goo.gl/ad5XxpxDDWv8qeQZ7
https://images.app.goo.gl/Amc3rCbYQcxXqhJk7

Remember that they will have their own dress and jewelry like this one.
https://images.app.goo.gl/YjsnbRLsknrtKA3r8
https://www.google.com/imgres?imgurl=https%3A%2F%2Fvignette.wikia.nocookie.net%2Faliens%2Fimages%2Fc%2Fcc%2FUrsa-maximus.jpg%2Frevision%2Flatest%3Fcb%3D20190403183841&imgrefurl=https%3A%2F%2Faliens.fandom.com%2Fwiki%2FUrsa&tbnid=SpiUHwaHn0hGSM&vet=1&docid=f1ymd_YXOBAWLM&w=650&h=345&itg=1&q=alien%20species&hl=en-us&source=sh%2Fx%2Fim

# Notes
Keep some things in mind about bilology. Maybe some are evolved from prey species and have their eyes further apart and
have to turn their head slightly away from you when you’re conversing.

https://www.google.com/imgres?imgurl=https%3A%2F%2Fcdna.artstation.com%2Fp%2Fassets%2Fimages%2Fimages%2F010%2F642%2F340%2Flarge%2Fmarius-siergiejew-alien-species-20170301-by-noistromo-x1811.jpg%3F1525456200&imgrefurl=https%3A%2F%2Fwww.artstation.com%2Fartwork%2FqDvmD&tbnid=lJHZzkMrXQxSeM&vet=1&docid=MGolASTvhqO2JM&w=1280&h=1811&q=alien%20species&hl=en-us&source=sh%2Fx%2Fim



# Notes About National Predelections
For each nation gotta define a number of things.

Their feelings towards humans
Keep slaves
Brands slaves
Power level (come up with a scale)
Can speak human
Language can be understood by humans.
Trade with humans
what else
how nationalistic? IE how likely those stationed here are to be persuaded by bribes, etc. Matemá is a full zero
the nest ones as well.


TODO: Move to Magic.md
There will be some people who don’t believe in Magic. Some of those may actually be pale sensitive but they don’t believe in Magic so strongly that their own magic manifests itself as just a big dampening field.

Maybe there are Daemons who have realized this and are taking advantage of it by capturing some of these folks and keeping them in matrix like environment where they aren’t aware of all the comings and goings of Daemons.
