# Language and Communication

# Slang
Need a lot of slang. Especially derogatory slang.

## Slang for People

  * Mechanic: One who uses magic gadgets because they are not Pale sensitive.
  * ?: Not Pale sensitive
  * ?: Submissive to their captors (Uncle Tom)
  * ?: Someone who's magic manifests physically. Implies that they aren’t smart enough to "do it right." Adepts and
  superheros fall into this category.
  * ?: Not pure human. Rumors of Daemons doing genetic experimentation make some folks think that anyone “weird”
  isn’t wholly human. Anyone with physical manifestations will automatically be considered this. This isn’t just some
  wacko bible thumper thing. Most humans feel this way. Maybe blue like they have blue alien blood. Maybe green for
  little green man? Maybe grey for basically the same reason. Maybe greyblood.
  * Tainted: Many humans feel that magic is a curse which stems from the same place the Daemons do since it didn’t
  arrive until after they did.
  * ?: Someone who uses technology. Some Practitioners feel that magic is the next evolution in our society and those
  who use computers and such are basically luddites.
  * Darkie: General term for practitioners, because they wield Umbra.

Look up some Latin, Spanish, and Chinese words for some of the things (especially the colors) and see if any are good
to derive a perjorative from. Like what happened with negro.


## General Slang Words

  * Krrrit: 'OK,' 'Yes.' tone rises at the end. A word in a Daemon language for yes.
  * Bright: Something super good. Since all the colors of the world are now somewhat muted and depressing. Bright is
  the opposite of that.
  * Clean: No problem. Because most humans live in squalor now.
  * Shampoo: something desirable.
  * Choff: Bullshit. Another Daemon word
  * Gaardung: Same, literally "Gaar Dung" Gaar being a species of load beast.
