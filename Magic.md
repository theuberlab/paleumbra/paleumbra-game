# Magic

Magic is arguably the most important thing in the world of Pale Umbra.

Magic is a technology which is known to exist only in the Earthly Dimension. This is because [Pale](ThePale.md) is
known to exist only in the Earthly Dimension.

There are some stories of lost dimensions which may have been caused by their inhabitants knowingly causing rifts
between their dimension and [The Pale](ThePale.md) and being unable to contain the resulting breach. The results being
that their dimension was absorbed by [The Pale](ThePale.md).

While humanity is often derided as incompetent by other Daemons for having created intra-dimensional tunnels before
inter-dimensional tunnels and no other dimension is known to have "accidentally" discovered inter-dimensional travel
the way the Earthly dimension has it is likely the nature of Earthly intra-dimensional tunnels and the synchronization
fields which kept stable tunnels open which allowed us to eventually seal the breach between our dimension and The Pale.
