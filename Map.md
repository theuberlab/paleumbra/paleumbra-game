# Map
Not an actual map of course but a list of locations which I need to place onto a map.



* The Laurentian Sea - A massive dry inland sea formed when the Nothing disintegrated
a large amount of land as well as the waters of lakes Michigan, Huron, Erie
and St. Clair
* Mt. Michigan - The remaining roughly 1/3 of the state of Michigan which is still above
sea level.



TODO: look up mineral resources and figure out what portions of land would be the most demolished.


# Map
The DRC is once again the DRC but it’s huge. Basically from where it is now all the way south.

The governing daemons don’t care how humans want to live as long as the work gets done.

Humans largely govern and police themselves there  they still work long hours in the mines.

They are even allowed by their masters to leave the confines of the cities surrounding the mines but if they are late
for their shifts the masters will put not only them but their entire block through the most horrible torture so it’s the
human government preventing them from leaving.

Most of southern africa is this nation except for the bit around the largest
[coltan](https://en.wikipedia.org/wiki/Coltan) mine where the Matemá have claimed it.

Madagascar is an island jail. One race doesn’t much care for earth but they are fairly dense and saltwater is corrosive
to them so it seemed like. Great place to build a prison. Their race is super strong and spit a sticky acid (not over
too great a distance) so they are tricky to imprison. Because the salt water is so terrifying to them they do from time
to time send prisoners of other species there (it doesn’t really occur to them that it’s not as effective a prison for
them. Plus it’s pretty secure otherwise)

Could be a different island like Japan.

On some other volcanic island (maybe one of the other Hawaiian ones) there is a cult who worships the old ones.
They chant and pray and try and get them to return. In some later expansion folks can perhaps make the maths and realize
that if these people keep it up they actually will attract one and earth might not survive. Maybe that’s a publishable
campaign.  Maybe Samoa would be a good fit for this?

Look up the mineral composition of earths magma. Something in it or better yet some specific combination is desirable
to some of the old ones. It’s like a well mixed cocktail to them. Not really a good food source just super tasty. So no
real strong desire to risk death by returning.


