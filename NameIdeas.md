# Names


# Names of Groups
Penumbra - A very large (probably the largest?) resistance group. From Penumbra (noun) the partially shaded outer region of the shadow cast by an opaque object. In Astronomy the shadow cast by the earth or moon over an area experiencing a partial eclipse.

# Names of places

# Names used to describe types of practitioners
Adept - A martial artist mage
Daemonologist
Mechanic - Someone not actually magical but with many magical (or Daemonic) gadgets

# Names of other things

Ummbra - The actual thing Practitioners use to make magic. Pale is absorbed by all living things.
Humans can convert it into Umbra which can create magic. Your Umbral Well is how much of it you can store.

Definition
Umbra
  the fully shaded inner region of a shadow cast by an opaque object, especially the area on the earth or moon experiencing the total phase of an eclipse.
    Astronomy:
      the dark central part of a sunspot.
      literary:
      shadow or darkness.

Origin
late 16th century (denoting a phantom or ghost): from Latin, literally ‘shade’.

