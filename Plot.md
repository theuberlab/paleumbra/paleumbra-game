# The Third Age

It was the beginning of the [Third Age of Man](TheAgesOfMan.md) No longer bound to earth Humanity had established a
colony on Mars. Desperate for a faster, safer, and cheaper method of interplanetary transportation created the Portals. 

In the years that followed Humanity entered a new golden age. But as hunger and starvation reach their lowest points in
recorded history the bright golden light of the Portals begins to cast long dark shadows.

Racial tensions begin to rise. A new era of violence is born as criminals and terrorists, unhampered by the fear of
extradition become more brazen than ever. As with all major new technologies fear of the unknown brings opposition.
Increasing tensions surrounding criminal use of the Portals spark protests and on the evening of October 23rd 2068
protestors park a van near the bricktown Portal in down town Detroit, MI. The van is packed with an ammonium nitrate
based explosive mixture similar to the one [used by Timothy McVeigh and Terry Nichols to destroy the
Oaklahoma City Federal Building on April 19 71 years earlier](https://en.wikipedia.org/wiki/Oklahoma_City_bombing).

Due to hightened security measures the Portal, while damaged, is easily repaired and returned to active service within
two months.

Unbeknownst to authorities and undetectable by the equipment at the time the Bricktown Portal had begun to exhibit
instability from the bombing. Due to continued usage of the gate the instability increases and on a cold january
morning 2069 while the portal on the Bricktown side of the Bricktown-Dong Gu (Busan South Korea) Portal binary remained
open, it's pairing field, collapses.

TODO: Maybe it's the other side which gets damaged?

# [The Pale and Beyond](ThePale.md)

It was this event which introduced humanity to [the Pale](ThePale.md) the place between dimensions.

What humanity had unwittingly accomplished was to become the only species in the known dimensions to have produced
intra-dimensional travel through the Pale without any knowledge of the Pale's existence or of other dimensions at all.

It may have been this very fact which was enabled humanity to save themselves from the ensuing disaster.

With the pairing field down on only one side of the tunnel but both portals intact for the first time a tunnel had
been created which terminated within the Pale itself instead of beyond it.

As the very essence of the Pale leaked in to the human dimension it destabilized and broke down into a thick black
fluid for which no species in the known dimensions has a name.

Humans gave it many names, Black Death, The Tar, God's wrath. These days most call it "Nothing," because that is what
remains where it has come into existence.

As Nothing coalesced out of the Pale which had flowed over the city of Detroit it began to consume the land around it.
It consumed the waters of nearby lake St. Clair as they flowed into the basin which had begun to form where Detroit
once stood.

Nothing continued to form from the thick layer of Pale which had spread across the state of Michigan long after
humanity had managed to close the doorway through which it had flowed. Continuing to consume everything in it's path
the Nothing eventually turned most of Michigan, the northern portions of Indiana and Ohio as well as the
waters of lakes Michigan, Huron and Eerie into what is now known as the Laurentian Sea. 

It was on the bed of this massive dry sea that the first interdimensional portals would appear. And through them
would step the first daemons humanity was to encounter. 

But before that was to occur Humanity would encounter something much worse.

# A Mountain Which Casts No Shadow

They are reported in the histories of all known dimensions. They are known by most as The Old Ones. Great beings so far
beyond our capabilities that they are no more aware of our actions than a stockbroker is aware of the actions of the
bacteria which inhabit his telephone reciever.

There is something about travelling betweeen dimensions which causes beings to be able to percieve the old ones. it is
thought, now, to be related to exposure to the Pale. Since the breach all of humanity is able to observe them.

The first appeared in the sky over the pacific ocean. The old ones are difficult to observe but Witnesses described it
as black as night and a cross between an octopus and a hurricane.

due to its size it was observable from the west coast as far as oregon until it drifted towards, and eventually
consumed, the big island of the state of Hawaii. 

Another, nearly twice its size appeared over the Norwiegen sea. It had the appearance of two birds constantly consuming
one another while snakes ate their eyes. It shone with a golden light which brightened as it consumed Iceland.

The last appeared on land north of Rapid city South Dakota.
Smaller than the other two, by all accounts barely smaller than the island of Japan. It appeared to be headed west when
it raised what must be surmized as it’s head and turned towards what had once been Detroit.

It cut a channel through south dakota, Minnesota and Wisconson before gliding across lake Wisconsin. Making land it
again plowed through city and field alike.

Until it reached the nothing. As the nothing began to consume its
body it let out a scream which instantly ended the lives of those nearby.
It managed to drag what was left of it's body onto a portion of northern Michigan which is now known as Mt. Michigan.
It is here on this island in the newly formed 'Laurentian sea' where a portion of its skeleton still lay.

It took nearly a month for the old one to die. As it did it continued to wail. A sound which would reach increasingly
more ears as 
Some say this scream which “Broke the sky.” 

TODO: A bit about colors not being as bright as they were. its actually from the pale in our air and land. 
Also a bit about how we closed the breach.


# The Great Daemonic Wars
Though they eventually learned these beings were not "demons" which arrived from hell the name stuck and the spelling
modified to the original Greek, daemon, which is pronounced the same. Beings which were not quite gods but were much
more than men.

For these beings were much, much more. Humanity had come to inter-dimensional travel accidentally, and much earlier in
it's technological development than most. As a result our weapons were no match for them and in short time word had
spread of a dimension of un-plundered mineral wealth peopled by soft inhabitents who could not defend it.

In the great rush of species that followed it wasn't long before competition turned to war.

Within the first year after the gate explosion and the arrival of Daemons most of humanity was already exterminated or
enslaved by the handfull of races which had taken an interest in our dimension.

As the Daemon conflicts began to wind down humans discovered magic.

This turned out to be both our salvation and downfall. While it made us capable of competing as a species, by then all
our social and military structures had been destroyed. The "Human resistance" typically ammounted to local extremist
militias, criminal organizations or small military groups disenfranchised when their government was destroyed.

However, as word of humanities newly found magic spread throughout the races of known Daemons our dimension took on a
look of shiny possibility. Hundreds of new Daemonic races arrived to stake a claim as they attempted to discover the
origin of, and to harness this 'magic.'

The Great Daemonic wars entered a period of renewed and increased conflict which would last (30? 100?) years. Even more
of humanity was killed. Non-practitioners and those not in living in resistance city-states were far from safe. Millions
perished in slave camps in Daemon on Daemon attacks. Or in the wreckage when a species gave up on this dimension and
"Salted the earth" behind them as they left. Think the gold rush except instead of individuals, families and small
companies each 'claim' was being defended by a race more powerful than any nation in human history.

# The State of Things

There is no single event which historians point to and say "That was the end of The Great Daemonic Wars." There is
little agreement upon when they ended. Ultimately most of the invading races, finding little of value and unable
to crack the code on magic cut their losses and left. Eventually more or less of a stalemate was achieved. 

Some 300 years from the collapse of the Bricktown-Dong Gu portal the Earth is forever changed. Scarred and stripped
of resources during the wars entire ecosystems have been destroyed or "terraformed" to match some races
(distinctly non-terran) home dimension. Other subjugated races, unable to gain a foothold in their own dimension were
able to scratch out a small place to call home on earth by aligning themselves with humans. 

Since humans are still in their infancy as a 3rd age species we are still considered backwater by most known daemons.
As a result there is still little land on earth which humans can exclusively call their own. 

Even humanity's allies do not recognize earth as inherently belonging to humans.

One of (the?) the first races to recognize humanities legitimacy was one of the first to show up and
one of the first to enslave humans (but not THE first on either count.) That warlike one and they don't recognize
hereditary land rights. Humans had to take any territory back by force. One day they just started letting us all go.
Any human who tried to reclaim their home was told to piss off. If the issue was pressed, they were slaughtered.

But from that point on they became willing to enter negotiations. And any prisoners captured during combat were
well treated and released after the conflict resulting in their capture ended.

It still took quite some time before anyone began calling them allies. 

Most distinctly human cultures have been lost. Humans who earned their freedom (through whatever means) developed
a hybrid culture by adopting much of the culture of their allies, or captors.

There is no human alive who was born before the wars. Much of humanity knows about it only as legend. Most human
resistances from the early years of the wars have either been destroyed or acquired enough of a foothold to gain some
semblance of legitimacy as a new nation in this changed world. 
