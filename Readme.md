# Pale Umbra

In the future a humanity desperate for cheaper space travel accidentally punches 
a hole into a mysterious realm known as the Pale. 

The Pale is the realm between dimensions and when it’s very essence flows out onto the
earth humanity learns that it is not alone. 

Hunted and enslaved by inter-dimensional beings with technology far beyond their own 
Humanity seems on the brink of extinction. Until they realize that they same event 
which brought so much death and chaos has also provided them with a new technology. 
One which humans alone can manipulate. 

They call this technology Magic. 

As a practitioner of magic. You will team up with other practitioners to help
reclaim the earth from invaders. 

But why?

Are you a newly formed unit sent on a rescue mission to free slaves destined for slaughter?

Or are you a band of mercenaries hired to destroy a local portal and prevent the enemy
from using it to re-supply their local troops.

Perhaps you are simply a gang of opportunists keep to make a buck by stealing
some high valued equipment from one side and selling it to the other.


Pale Umbra is a tabletop roleplaying game which emphasises storytelling over rigid game mechanics.

Help humanity return from the brink of extinction, or profit from their demise.

# [Main Storyline](Plot.md)

# System
Initially Pale Umbra will be played using the rules from Hero System Sixth Edition.
Specifically as they are laid out in [Champions Complete](https://www.amazon.com/Champions-Complete-Derek-Hiemforth/dp/158366145X/ref=sr_1_1?dchild=1&keywords=champions+complete&qid=1634073228&sr=8-1)



# Language
during the wars humanity became spread all over and jumbled up. One might be able 
to read and write... in one’s native tongue, but what is spoken around where they 
are right now. It can differ not only city to city but street by street.

As humanity tried to organize and fight back a fairly common spoken "Human" language developed. 
largely a combination of english, Mandarin and Spanish but there is little consensus 
on a written form.

So lots of names like “The Mollusk and Barrel” which can be represented simply by pictures. 

Some (very few, none playable) species are for some reason unable to learn human. These can 
probably speak some other specific languages (like Arabic or Thai or whatever) and might actually 
get offended if some speaks human to them.


