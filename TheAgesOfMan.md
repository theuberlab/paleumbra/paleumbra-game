# The Ages Of Man

Most daemonic (inter dimensional) societies define three ages for humanity. Different dimensions go through similar phases but at differing times and rates based on environment, temprament and physiology.

For humanity they are typically defined as follows:

# The age of firmament
The first age is defined by our attachment to the ground.

# The age of Flight
The second age is defined as the period from the beginning of flight, through space travel until the advent of the portals.

# The Daemonic Age
The daemonic or interdimensional age (relatively few call it the modern age) is the age of interdimensional travel and trade.

There is some argument amongst scolars about the specific beginning of the daemonic age. While most credit the creation of the portals, others argue that our early inability to travel into another real dimension places the start of the daemonic age at the beginning of the bleeding(or whatever that gets called when the dimensions bleed into ours.)

# The “Fourth” Age
It is perhaps due to this ambiguity (though likely due to simple malice) that some historians place humanity in its’s fourth age. The third, often called The Aborted Age, being the relatively few years of purely intra-dimensional portal travel.

# Alternative Namings
Some species refer to the ages differently.

The Age Of Fear - When we were largely ignorant of one another
The Age Of War - When we were killing one another
The Age Of Death - Because in modern times we are out-classed and being killed by other species.

