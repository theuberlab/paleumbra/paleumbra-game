# The Old Ones
"The Old Ones" is a common name used to refer to a group of Inter or Qasi-dimensional beings who's
capabilities and (estimated) intelligence is so beyond that of [known Daemons](DimensionsAndDaemons.md)
that [known Daemons](DimensionsAndDaemons.md) may not even be recognized by them (or even percieved by them)
as living beings. Similar to the way humans are incabable of percieving bacteria without the aid
of technology and were unaware of bacteria's existence until said technology was perfected.

Other names for them include "The Old Gods," and "The Great Ones." The name is derived from speculation
that they have existed before any of the races of [known Daemons](DimensionsAndDaemons.md). This is
still considered speculation because while they are recorded in the history of all inter-dimensional
species. Any attempt at an accurate timeline is hampered by the fact that their appearance is not
only rare but they can typically only be percieved by those who have traveled between dimensions.

It appears that this is due to exposure to Pale because all humans appear to be able to percieve them.

# Appearance and Diversity
It is unknown how many species of The Old Ones exist since they have been observed in all manner of
forms including beings which seem to be made of pure ilght. Typically they are reported to be larger
than the island of Japan.

It is unknown how The Old Ones are percieved but it is known that they are uneffected by light since
they cannot be recorded and they cast no shadows. 

They also appear to be unafected by gravity however they do seem to interact with or at least be capable
of interadcting with known matter since there are multiple accounts of the destruction they have caused.

# Occurance
It is unknown if the Old Ones travel to dimensions which have not achieved inter-dimensional travel
since they cannot be percieved by non-travelers.

# Worship
Due to their omnipowerful nature The Old Ones have been worshiped by all [known Daemons](DimensionsAndDaemons.md).

# Death and The Nothing
There has only recorded death of a Old One. It occurred in the first year after the breach. It occurred
when an old one, possibly attracted by the breach, appeared in our dimension and and was killed by
the giant pool of Nothing which formed [the Laurentian sea](Map.md). A portion of it's skeleton
fell onto the area which at the time was Northern Michigan (now Mt. Michigan.)


# Need to create a section for notes that won’t be published.
There are several races of the old ones. They may have made the dimensions.
Do they live in the pale? That’s why the nothing can destroy them? That’s why they were attracted to earth because it
was full of Pale?

Maybe they feed on Umbra and made dimensions to collect it? So the various known daemons are all from dimensions which
just haven’t been harvested yet. The species in each one has to get to a certain point before they can do it. Exposure
to Pale over millennia makes it happen. But on earth the combination of constant exposure and the death of the old one
triggered it in us. The old ones haven’t decided to come harvest us yet because they haven’t noticed? We weren’t
producing Umbra by the time they stopped coming.
