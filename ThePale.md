# The Pale

The Pale is the name of both the realm in between dimensions and a substance (known 
simply as "Pale") which originates from it.

At even relatively low concentrations The Pale is highly unstable and breaks down into a substance 
(called Nothing) which is extremely corrosive and highly toxic to all forms of life in all dimensions. 
It cannot be contained by any material known to any of the known races (at least any of the communicating races.) 

At extremely low concentrations however The Pale is the basis for a technology exclusive to
the Earthly dimension. On Earth this technology is commonly referred to as Magic.

# Pale
Pale is completely invisible and able to easily pass through most forms of matter. It is
known however to pool in living tissue and certain substances not native to the Earthly
dimension. 

This allows certain individuals, known as "Practitioners of Magic" or simply "Practitioners"
to store Pale for later use. It is un-known how much Pale can be stored. It is also unknown
if stored Pale will reach high enough concentrations to break down into Nothing (see below.)

# [Pale and Magic](Magic.md)
Those attuned with Pale are able to use it in a number of ways. This practice is known as "[Magic.](Magic.md)"
It is speculated that the forms of Magic are limited only by the imagination.

# The Nothing
Nothing is a substance which destroys all life and breaks down all known forms of matter. 
It acts as a thick viscus liquid. Since it absorbes all light it is flat-black and very difficult
to measure. It's appearance has prompted some scientists speculate that it's destructive capabilities
extend to all forms of electro-magnetic energy. Though most discount this theory as "Absolute toddle."

Since it cannot be contained, and thus cannot be transported and has never been witnessed 
in quantities larger than a few milliliters there has been no direct study of it.

## Names
The nothing is known by a number of different colloquial names including: Nothing, The Black Death
The Creaping Death, Deathwater, Antimatter, and more.

In scientific communities it is most commonly referred to as "Erasive matter."

## Erasive Matter
The single saving grace of Nothing is that as it breaks down matter it appears to also brake down.
It is not known if it breaks down into elements common to our dimension, if the process of
breaking down Earthly matter causes it to re-form Pale, or as some speculate, is transported 
back to The Pale.

# Ley Lines

Pale is effected by a force (referred to as the pale force) similar to magnetizm and
concentrates itself into lines which span the Earth. These lines can be imagined similar 
to the lines formed when iron filings are exposed to a magnetic field except constantly in motion
like rivers which encircle the planet. These "rivers of Pale" flow all over the globe
typically near the level of the surface. While some have been documented significantly below
the suface none have been documented more than a few hundred meters above the surface.

It is not understood what keeps Pale near the surface or what keeps it moving 
However there is some speculation that the pale force is emited by dimensional Portals and
Ley Lines are the resulting effects.

## Tapping Ley Lines
Those attuned to Pale are usually able to learn to draw it from Ley Lines. This process is 
referred to as "Tapping" by most Magic practitioners. The process is described as "like
trying to drink from a river using a sock." since it is both laboreous and slow.

### Risks Associated With Tapping
The most common adverse reaction to tapping a Ley Line is simple exhaustion however
some individuals have reported "Pale Burns" from drawing Pale from a Ley Line too quickly.
Pale Burns are a form of radiation burn identical to the common sunburn but which typically
occur in circular patterns at the center of the forhead, the back of the neck and the fingertips.
They are also known to occur on the palms of the hands and less commonly the feet.
All sights which practitioners commonly visualize as the points where they pull Pale into 
their bodies.

Due to the difficulty typically associated with tapping Ley Lines many practitioners dismiss
such stories as urban legends. 

## Pale Storms
While generally stable Ley Lines have been known to shift, dissapear or appear suddenly.

This is a relatively rare occurance typically happenning less than once a week anywhere 
on earth. No technology currently exists to predict it. such an incident is immediately 
percieved by Pale sensitive individuals (regardless of their ability to practice magic)
near enough to tap the Ley Line in question as it instantly disrupts the Pale they 
have stored in their bodies.

These events are referred to as "Pale Storms" and while they usually result only in minor
disorientation, discomfort, and loss of stored Pale. It is not uncommon for practitioners
to report accidental castings during larger "storms."

## Nothing Lines
Very rarely (to date there have been 17 reported occurances) Pale in Lay Lines can achieve
high enough concentrations to become unstable and break down into Nothing. Nothing lines
are typically only a few (1-5) meters in length. The longest reported Nothing Line was 
approximately 238 meters in length.




