# Stories of a Small Number of Existing Practitioners

To help set the tone of the game and to give players some ideas.



# CrackerJack

Before the breach you were a god among men. You had been on every continent
on the globe and whatever they had on mars. If there was a computer there connected
to the internet you could get in to it. 

Your body seldom left you room but who needed to? You were connected. The internet
was your playground. Armies of cyber minions coded by you tunneled all over the
world sniffing out any breach, and where they did they planted their eggs. Eggs
that would hatch quietly, discretely. But they didn’t just send out more worms
they infested the minds of the machines they occupied, and they reported back. 

Through layers of indirection data flowed to you from all over the world. At
each layer it was collated and transformed. It hopped to networks of hyjacked
technology which formed a massive supercomputer which only you new existed and
there it became information. 

Computers were great and data was cool. But information, information was power.
What were companies protecting from other companies. Who was buying whom. Which
superstar had been caught on a camera they were too stoned to know was there?
And most of all who would pay for that information?

When the gate exploded you knew instantly. Channels of communication once dormant
became saturated and there was nothing preventing you from listening in. But you
cared more for the breaches they left when they opened new channels to collaborate.
As long as they managed to clean up the creeping black mass that had devoured Detroit
before it got to your apartment.

You heard the reports of the mysterious creatures which appeared in the skies
in the months that followed. Saw the distorted drawings and watched videos of
blank skies while some panicked sheep rambled their description of a thing the
camera could not behold. But you watched in awe as the island of Hawaii lifted
into the sky. Followed by great streams of magma which rose into the sky as if
through an invisible straw. And in horror when they decided to nuke the second
one. Saw the giant missle fly towards an empty sky somewhere north of England. 

You had begun analyzing data from weather satellites and disruptions in communications 
signals but nothing in your toolbox could detect them. Not until the nuke detonated. 
You saw that as clear as the coffee maker on your desk. And then Iceland suffered
the same fate as the big island.

You learned of the creature in North America watched the great channel open up
as it moved across the land, saw trees flug aside and huge tufts of earth flung into
the sky by, you had to assume, it's feet. But still you couldn't observe it.
Only those in person could see it and hear it.

But you heard it when it died. Everyone did. You left you apartment that 
day and wandered the streets. Shops were closed, windows boarded like some sci fi 
apocolypse. Suspicous eyes peered through blinds. 

And still the information flowed. Back at your desk you learned of the increasing
riots, not just from “the end is nigh” looters but from the mysterious rash
of extreme violence and canibalism. Alerts indicating new exploitable software
vulnerabilities showed on your dashboards and fell on blind eyes. For the first
time you didn’t feel the need to update your cyber armies. Your attention
instead turned to humanity. You wrote new algorithms which predicted patterns
of behavior in what the media were calling “zombies” (a stupid name for them
since they weren’t dead.) 

You knew something was happening in the great crater they called the Laurentian Sea.
You had a camera equiped drone there by the time the first portal opened. You gasped like
an old lady. This wasn't the travel portals you had grown up with. Binary pairs 
which held open each end of a tunnel you could step through. 

These portals stood free. They lacked the ring which held the portals stable. 
And the huge building of equipment which provided their power and cycled the 
layers of interacting fields which kept the portals open and linked. Nowhere to 
be found were the mass of operators endlessly moving back and forth on their short
sub hour shifts, working then leaving before proximity to the portal caused 
hallucinations so bad they had to be hospitalized. 

And when the... Demons, Aliens? they couldn’t be called anything other than that, 
when they stepped through them you forgot about money and power entirely. You told
the government of your findings. Of how by coralating the data from military and weather
satelite networks from various countries could detect these new gateways before they
opened. 

And when your own government came for you, you learned that altruism was folly.
That you had been right all these years. Humans were sheep and they were beneith
you.

So it was a relief when the <SOME RACE> came for you. When they tore the bars
off your cell and herded you into one of those terrifying portals to arrive in
one of their "colonies." Captured human cities with great walls built not to
protect, but to keep you in. You heard stories about other races using humans
as food or working them to death in mines. Your captors knew that if they kept
you alive they wouldn't have to hunt as many humans. Or kill as many who tried
to revolt. 

You still spent your days in a mine but you left them at the end of the day.
You returned to a cot which was yours. When they started handing out the little
glass panes which acted like the old cell phones you knew it wasn't out of
the goodness of... whatever pumped their blood. It was another method of control
So that your captors could track you and know where you were every second.
So you resisted. But the sheep were now connected. They could talk to humans
in other colonies, they could see loved ones. And when you saw the first meem
spread like a virus you could no longer resist. 

So you picked up your assigned pane. And when you touched it something happened.
It new your desires. It wispered to you of pathways and worlds, of gates
which could be opened and walls which would crumble before you. You'd
been trying to learn the <SOME RACE's language so that you could learn to
hack their systems but that was no longer neccessary. This network traveled
directly through the Pale and from there into every machine the <SOME RACE
had built. You couldn't read there language, you weren't even sure how their
input devices worked but it no longer mattered. You travel into their computers
you could see the logic structures as easily as cabinets and desks in the "real world"
You could see their security travelling towards you like bumbing cops in a
child's cartoon.

And like before you were everywhere. You broke down the barriers between the
networks of your captors and the networks of their enemies. Like you, you
couldn't speak the other's language well enough to hack their software
but you didn't need to. You were IN their computers. But unlike the old world
this didn't require a keyboard and screens and a mass of humming machines which
all tied you to the chair where you grew fat and weak. All you needed was
your... Deck. The pane of glass which connected you to this new cyber world.
It fit in your hand and you could take it anywhere. So when you learned of
an impending attack you positioned yourself near the wall they would breach
And when they did you watched through the cameras of your captors enemies
until you saw an opening and you ran. 

There was a new world out there. A new world of new data. And you were the
one who would turn that data into information and that information into power.



# The greatest potato
You watched too much tv as a kid. Heck you watched too much TV as an adult. But you 
loved Super heros and magicians. You imagined yourself flying in a halo of golden
light. A light that would concentrate in your fingertips and when evil was afoot 
bolts of golden lightning would leap from them and ensnare the evildoers. 

When the Daemons arrived in town you ran like the others. But it was no use. Those
flying drones and the red light which shone through walls. Eventually they netted
each and every one of you and threw you in cages. They marched you into pens and
headed you around. You had water and something resembling bread which you used
to sop up the filth they gave you as food. You dreamt often of your old life. You
all had dreams. And you shared them. Yours were of your comfortable sofa, of a
well stocked fridge and food that was warm. Even of your sister lorraine reading
you the riot act for what was easily the seventh time. There was the skinny kid
who dreamt of the girl he had been trying to build the courage to ask out. But it
was when they came for the old woman that things changed. When that putrid smelling
grey beast wrapped its tentacles around Birdie. Sweet old Birdie who dreamt of the
cats she wasn’t able to feed. And of the grave of her late husband which she had
visited every day without fail since he died nine and a half years ago. When it
lifted her out of the muck and turned to leave you wanted to do something to leap
on it and pound it with your fists but you hesitated.

You’d seen what they did to those that resisted. You could still see the limbs
and torsos spread around the head of one unfortunate lay in the muck not far
from your feet. It’s eyes judging you. “You’re not even willing to try” it said.
Frustrated you remembered a better you. The you of your dreams. A you that could
stop these beasts you clenched your fists in rage and screamed. The lumbering
creature turned towards you, as it did it drug Birdie's body through the mud.
When it met your eyes you opened your fists.

Somehow you were unsurprised when golden lightning lept from your fingertips and
wrapped around the beasts throat. More lightning shot out wrapping around its
tentacles and gently prying birdie free. 

The beasts eyes bulged. In horror, in fear, who could tell. Perhaps it was the
lightning getting tighter around its neck. A tentacle shit towards its waist and
grabbed a cylindrical object it wore there. You didn’t know what it was but today
was not the day to find out. The beasts body crumpled as it’s head toppled, freshly
severed from its body. 

The others ran to Birdie, she was bruised but alright. As she raised her eyes to
stare at you with wonder so did the others. When they did you realized why they
were staring. It wasn’t the lightning, it wasn’t toppling one of your captors.
It wasn’t for what you had done. 

It was the golden halo. The others were staring at what you would do. They could
see it. And now you could too. As you rose from the mud your golden halo grew
brighter. But even as they shielded their eyes you could see their smiles. Their
relief. It was time to leave. So you took to the air over the camp and began
cutting a path through the fences. A path to freedom. 

# The Pupils
As a martial artist it was just as important to you to keep your mind clean
as your body. How else would you channel the chi in your body to the destructive
force it could become. Of course you knew visualizing the chi flowing from the
earth into your body and forming a blade as your fist struck was just a mental
trick to help you focus your punch better. Until the day the ice blue blade
actually appeared and your lovingly crafted makiwara exploded. In your shock
the blade shattered. Stunned you tried to conjure the blades without success
for three days. You doubted your sanity. Until one day running your hand over
where the makiwara now ended you felt a prick. The splintered wood had embedded
itself deeply.

You extracted it calmly with a pair of tweezers from your bathroom cabinet
and you bandages your finger you knew that this was real, as real as this temporary
world can be. So you returned to your zafu, your meditation cushion and you
regained your focus. You stood and returned to your training room. The blades
came easily now. And there was more. You conjured shuriken which sliced through
anything you threw them at. And when a wall you’d just partially destroyed began
to topple you drew chi from the earth and your body began to shimmer. You
caught the wall with ease. 

You had heard of the invaders. If whole cities claimed all over the globe.
They had not yet reached your small backwater town but you knew it was only
a matter of time. So you took to the road you found other martial artists
like yourself and tried to teach them what they could do. Many resisted but
some carried a spark. You learned to feel the ones who could be taught.

They called you Sifu and Sensei and you let them. Because your belt no longer
mattered. You  were no longer learning the martial art of your former master.
This was something new. You had to define it, and if humans were to survive you
had to teach it. They began calling themselves “The Pupils” So you taught them
honor. And you taught them a code. You taught them that all earthly life was equal.
And all earthly life was to be protected. But there was life on earth that was
not of earthly origin.

So you taught them tactics and the value of protecting your lines of supplies.
And together you began to teach these invaders the same lesson. 

# Fresh Air
When other races first witness magic they assume the humans wielding it have 
acquired some Daemonic weaponry. But some of the smarter ones are beginning to 
catch on. And they want it for themselves. 

You are subject a46g278 at least in English. In the language of the Subjat it 
actually sounds pretty, but it still isn't a name. 

You are relatively certain you were grown in a vat. Human DNA mixed with a Gorn. 
A type of herd beast from the Djuwelli dimension. A beast they could control. And they did. 

At their command your horns would glow with a white light and you would slash 
through whatever they put in front of you. Wood, metal, beasts. Then one day they 
presented you with a new beast. There was something familiar about its smell. About 
its shape. It didn’t have horns or scales but it had hands like yours. Hands like 
you had never seen on any other creature. But it was the face you see in your 
dreams the face it made when you moved to slash through it. You knew that face 
because you made it yourself. When your keepers approach you with a blade to test 
how quickly you can regrow a limb. That face was fear. Looking down at its lifeless 
body at its severed arm laying in the dirt just like your own had so many times. 
And at the blood. Red blood like your own. Not the blue of the beasts you’d killed before. 

You did not know what this creature was which felt the same fear and bled the 
same blood. But you knew there must be more. And you knew you were closer to 
them than your masters. So the next time one took you from your pen for grooming, 
you quickly sliced off his head. Before he could reach the rod at his belt. 
The one that controlled the collar around your neck. The collar that could make 
it impossible for you to produce your blades. The collar that could fill you 
with pain if you disobeyed. And the collar which you were certain could end your life. 

So you ran. You cut through stone and steel and wood until you breathed fresh 
air. And then you ran more. As the alarms raised behind you, your jaw clenched. 
You waited for the sting of the collar. But you felt no pain and you did not 
die. So you ran. In the clear air of the night you ran. You wanted to drop to 
all fours and run faster but the flesh of your fore-paws was too soft to run 
on. And your back was too long, or too wide. The movements you knew in your 
heart were right didn’t come. So you returned to two legs and you ran. And 
you adapted the instinctive movements which drove you and you learned. 

And into the night you ran. Until a scent on the wind gave you pause. A familiar 
scent. So you followed it. The scent led you to a group of beasts. Of the beasts 
with fore-paws like your own. These beasts were clad but you knew beneith those 
layers there would be the soft pink flesh so unlike your own but which bore the same scent. 

They made sounds with their mouths which you did not understand. They were nothing 
like the screaming cries of your masters when they wanted you to cut. Or the rasping 
trill when you were to return to your pen. The sounds these beasts were almost 
pleasant. But when you approached they made the face of fear. Perhaps they could 
smell the one of their kind you had slain. And when you raised your dore-paws 
to signal that you were not a threat they also raise theirs. But their fore-paws 
were not empty. Their fore-paws were holding guns, which they fired. 

Two of the five went wide. One hit your shoulder tearing off a large chunk of 
protective scaling. One hit one of large plates on your stomach you felt it 
break three of your ribs but it drew no blood. The third landed just above 
your hip and just below the lowest plate on your stomach. It bit deep 
and red blood began to flow. 

So again you ran. For the night you took shelter in an abandoned structure. 
You found cloth and tied it tightly around the wound to stop the bleeding. 
In the morning the scales had regrown and you could tell the ribs were mostly 
healed. So you set out again. You crossed the path of three more groups of 
the pink beasts. You tried to approach each of them but this time making your 
presence known from further away. Each Time shots were fired so you stayed away. 

The wound on your hip had healed but something wasn’t right. It was swollen and 
yellow. Running no longer gave you joy so you walked. And when you next smelled 
the pink beasts with the blood and the fore-paws like your own you began to 
turn away. Until you heard the cream of wheels. The creak you heard every time 
they brought new beasts for you to kill. The creak you had heard just before you 
were commanded to kill one of the pink fleshed beasts. So you followed the sound 
and the scent until you found a caravan of five wagons. Four of which were 
filled with the pink fleshed beasts. As the caravan rounded a bend you lept 
upon the first cart and tore its driver in two. 

Then you scrambled over the top of the direst cage and lept upon the second. 
By the third the drivers knew something bad was happening. But it wasn’t 
until the fourth that they rallied their defenses. As you gored the fourth 
driver the fifth driver shot you in the chest cracking your sternum. Looking 
down at the dead master before you, you lifted his gun and threw it at the 
fifth master with all your might. It struck him straight in the nose bridge 
giving you the time jump on it and sink your horns into its fleshy throat. 

You freed the pink beasts from their cages and confused, they stood staring 
at you. Then the started making those same sounds with their mouths. You stood 
there at tried to convey that you didn’t understand. They began moving away
but a few of them strayed and beconed you to follow. 

By nightfall you hip was definitely not a normal wound. Half way through the 
next day your eyes blurred. You woke up on a palanquin pulled by two large men. 
One of the softer of the pink ones was holding a plant to your hip. It made 
you forget the pain. 

And then she raised a knife and you realized you were tied to the palanquin
helpless when she plunged it into your hip. After a few excruciating moments 
too tired to resist you waited for death. she produced the 
object which had been embedded in your hip and produced the offensive results. 

You stayed with this group of “humans” for many seasons. You learned their 
language. You met an old man who taught you of genetics and experimentation.
He showed you the Gorn, now plentiful in these lands and how you had been 
built from pieces of them and from humans like himself. 

So you travel, alone. From time to time you meet "Practitioners." Humans
who, like yourself can bend Pale into Umbra and do fantastic things. Some
of those who don't try and kill you on sight will fight by your side for a time.
Until it's time for them to return home. To meet other humans. And thus time
for you to move on.

You leave messages for one another. In this way you learn of new missions.
Of new opportunities to kill the monsters who made you. 


# Secret Agent
In the first few battles we managed to capture some alien weaponry. 
Scientists have furiously been trying to understand it and they think 
they’ve made a breakthrough. You are outfitted with the most advanced Daemonic 
based technology your superiors had to offer. Now they want to know how it performs. 
“Go out and kill a few of dem horned bastards” way out, out far enough that if 
any of your fancy gear explodes it won’t damage the base. You make sure your body 
camera and other recording devices are on at all times. And you ship back the data 
they’ve recorded along with your reports without falter. This could be what tips the 
balance and you owe it to your country to try. 


# Better than you could have been
They (though you’re not sure who) had the technology to rebuild you, and they did. 
They spliced in some alien tech and now your arm terminates in a pulse cannon. 
The cameras which have replaced your eyes can see radio waves, xrays and more. 
And a mesh of a silvery alien material weaves its way through your now nearly 
indestructible skin. 

# Built not Bought
You were always a tinkerer. But one day your gadgets started doing things
you didn’t think possible. Now you are never found without your trusty toolbox
a pack pack full of components. “What do we need? Rocket boots?” “Sure, just
let me scavenge this vacuum cleaner and that pulse rifle. Tear down that curtain
for me. Then get me some Bobbi pins and give me ten minutes.”

# The Dreaming
Your white friends were nice enough. But whenever you talked about the Wangarr
time you could tell they thought it was crazy. But you knew it was real. You
FELT it as strongly as you feel the air.

Then one night the Djang'kawu Sisters visited you in a dream. One gave you a
vision of a rangga stick buried deep in the outback. The other taught you how
to sing the names of things. That morning you filled your pack with some dried
meats and bottles of water setting off south just as the sun creates the horizon.
It was an ardurois journey. You were no stranger to wandering these lands but
this was not the season one should do so. Not if one’s intent was to live. You
rationed as well as you could but the meat went first. When the water went you
kept your eyes pealed for certain dried brush. Digging up its roots with your
machete you crushed its pulp in your hand letting the stored water drip into
your mouth. It wasn’t much but it was enough for now. You were close. 
You began to hear the sound of scraping stone and out of the corner of your
eye you thought you saw the smoldering eyes of a malignee. But it didn’t matter,
you fell to you knees and began to dig with your machete. Around you more eyes
came in to view. The Malignee were usually solitary and avoided humans. But
they new something important was happening.

With most of the last of your strength you pulled the rangga stick from the
hard clay and rose. 

The clacking and cringing of the malignee’s knees suddenly stopped but their
eyes grew fiercer than before. The blades of their knives glinting in the light.
You poked the rangga stick into the earth at the base of a large stone and
though your lips cracked and bled and your tongue stuck dryly to the bottom of
your mouth you managed to sing the name of the stream.

And as you did it began to flow. The Malignee howled and danced away into the
night. So you drank your fill and being hungry you poked the rangga stick into
the river and sang the name of a fish. Catching it easily in the small stream
you built a fire. This you built with wood and lit with flint. Fire already
had a name and your voice was tired. Sated you lay down under the stars. You
thought you heard the rumble of Mamargan’s voice in the distance but you were
unworried. The world was changing. And things would be different for your
people in the morning after you slept. 


# Bought Not Built

Before the breach you had everything. A great house, fast cars, a beautiful wife. 
But you took none of it for granted. You weren’t some silver spoon trust fund baby.

You were born in Iraq and while you loved the land of your birth things there were a 
little... unsettled. Years of war had wrecked most infrastructure and due to bad 
relationships with its neighbors Iraq had taken to drafting young males, by force, at 14.

Your father didn’t want to see this happen to you. Or to have his newborn daughter 
killed by an American missle because some terrorist was drinking tea in a shop next door.

So before your tenth birthday he packed up his family and moved to America where 
he had a friend who owned a grocery store who could give him a job. 
Your family lived out of a car for almost a year. Your baby sister was swaddled
in newspapers because you couldn't afford diapers. 

You saw how much they had in the store. And you saw the way the whites looked at you.
Why did you have so little when they had so much. So one day you swiped a box
of pampers and gave it to your mother. "I found them behind the other store."
Of course she didn't believe it. Your mother cried. Your father was furious
"No son of mine.." he started but you cut him off. "I've seen the way they treat 
you. Why do they deserve so much when they treat you like a dog." You thought your 
father was going to breath fire. He raised his hand. You closed your eyes waiting 
for the strike. You knew it was wrong, and that you deserved it. You just 
couldn't stand to see your sister and your mother suffer.

But the hand never fell. When you opened your eyes he was still standing over you.
the veign in his forhead pulsing, the fire in his eyes undiminished. "No"
he said. It rang of finality, you never questioned it. "No, if we are to succeed
in this country, we need to do it by their rules." He took a breath "No-one
is going to stop us, no-one is going to prevent us from living well. But we
will do it within the law."

He handed you the box of diapers and took you to the store that night. 
You walked up the narrow stairs in the back to the office where the owner was
working. The owner smiled when you entered but the smile quickly faded when
he saw your father's face. Your father said nothing he simply pointed. You handed 
the diapers to the owner and stepped back. The pudgy balding man looked perplexed.
He looked to your father for an explaination and you followed his gaze. Your father
simply started at you. 

You knew what he expected. You had seen the same look when you broke your mother's
favorite planter before you left Iraq. You had created this mess and you alone 
would clean it up. "I stole that from your store last night." You managed to choke
out. The balding man looked from the box, to you, to your father. Dissapointment
on his face. He had given your father a job, had trusted him and you had betrayed
that trust. You knew the decision he now wrestled with. Should he fire your father.
How could he know you wouldn't do it again. Or that you hadn't learned it from him.

So you stepped forward forcing the man's eyes back on to yourself. "I did this
alone, and I know it was wrong. Call the police if you must but don't punish
my father. He has a wife and a little girl to support." His eyes raised.
"A wife, a little girl and a young boy, you mean?" a meaty finger pointing at
your chest. 

"No." You did your best to give it the same finality your father had. "No, 
he won't be supporting me anymore. After I get out of jail I'm going to get a
job." First I'll pay you back for the diapers and then I'll earn enough to buy
them from you. The man failed to surpress a smirk and you wondered how he could
be so cruel to laugh when you were going to jail. "Son" he said. "I'm not going
to send you to jail for a pair of huggies." And I think your parents might
have something to say about you working instead of going to school.

Your father agreed but you were insistent. You were going to help support the family. 
So your father made you a deal. Part time, after school but only if you kept
your grades up. 

You worked alongside your father at nights. You became a problem solver. When
a pipe burst and they were unable to get a plumber who would be out before
the morning you rode your bike home, where you'd seen what looked like plumbing 
equipment in a neighbor's garage. He was retired but but agreed to help.

At college you continued. You bought text books from graduates for more than
the store paid and sold them to freshmen for less than it charged. You established
a network of expertise, when someone needed a calculus tutor you knew exactly
who they could get help from.

After you graduated you started businesses and sold them. Companies hired you when
they were loosing profits. You showed them how to get more done with the people
they had instead of saving costs with large layoffs. You never forgot the humiliation
of your baby sister swaddled in newspaper. Or the shame of returing the diapers.
You never forgot the stench of being poor and you would never experience it again.

When the gate exploded and the world changed you lost everything. When that
giant monster died in Michigan your wife changed. She attacked you and ran off
into the night. When the <Some Raceherded you all into cages you were separated
from your sister. The two of you watched as they killed your father who was too
old and weak to work their mines. At least your mother had been spared the sight,
taken by cancer 3 years prior.

You toiled in their mines for years. Helping people, making contacts. You 
smuggled food, cigarettes and alcohol amoungst the prisoners. But you couldn't
solve the biggest problem of all. Your captivity. 

When you heard of humans with great powers you dismissed it as a myth. Wishful
thinking by people too downtrodden to face reality. But when one of them stood
up and carved a tunnel through the stone walls of the mine with a great blue light
which shone from his hands you saw your answer. You followed him out. And on the
surface you followed him. He found others of his kind. Some who already had developed
their own "Magic" and some who were unaware but could be taught. 

You traded for him and made deals. He tried to teach you but you couldn't feel
the Pale which he assured you was there. Couldn't coalesce it into a force you could
wield. Once he even drew this force, a shadowy black substance he called Umbra,
from himself and forced it into your body. You vomited and passed out.

He suggested you leave. Staying with him would only torture you. Go find a
group of survivors, try and live a normal life. But there was no more normal life.
And you would make these creatures who had taken your wife and your family pay.
These hideous beasts who returned you to poverty would get what was coming to them.

One way or another. But if he couldn't even place Umbra into you he was certain
you would never be able to coalesce it within yourself. "Does it have to be in
ME?" You asked. He didn't know, he had never contemplated it before. Together
you forged tools he could imbue with Umbra. When he did it left a pattern. A
pattern of whatever spell he focused on when he did it. He gave you rings
to protect you and a wand with which you could strike back.

After he died you continued his journey. You found other practitioners with spells
of their own you brought them together into forces which could make a differnce
and you bartered with them for the creation of new equipment. Boots which 
allowed you to walk on water. A jacket which would cloak you in shadow.
Even a belt which allowed you to take to the air.

But as they grew in power and organization those who could wield umbra began to
think themselves apart from those who could not. They called you a "Mechanic"
Because you could only touch the Pale with tools. So one day you set out on
your own. Somewhere in a camp out there was the last remaining member of your
family. Your sister. You were not sure how but you would find her.
And you would make those great grey beasts who captured you pay.



# Game Theory

It’s name was misleading. It had little to do with actual games and everything 
to do with resolving conflict by giving both parties an option which was better 
than resisting one another.

And you were good at it. Your skills had taken you all over the globe negotiating 
business deals in Asia, the Middle East and Europe.

But after the breach that came to an end. You weren’t sure if there even still 
was a place called Europe. You’d been negotiating mineral rights in the DRC when 
they landed or appeared or whatever it was called.

So you went down into the very mines you’d been there to negotiate. The newcomers 
had trumped all issue of ownership with massive cannons and a fleet of flying 
disks which leveled the surrounding forests leaving nowhere for resistance fighters 
to hide. 

So you toiled alongside them in the mines. Alongside the <list a bunch of different 
ethnic groupsyou watched as your captors wipped old and young alike. Pushing 
until someone dropped then leaving the body until the other humans moved it away 
to keep from tripping on it and getting the lash themselves. And you knew there 
was a better way. But it was hard to negotiate with someone when you couldn’t 
speak the language.

So you learned it. You listened and watched. You paid attention to their signs 
and control panels until you began to understand. And you practiced, quietly 
at night. The deep muddy sounds were difficult to duplicate but you persevered 
until you were ready.

The first time you tried it you thought you were dead. The <some raceturned 
and spread its mandibles in an expression you have since learned indicates 
shock amounts their people. This expression still terrifies you to this day.

Somehow you managed to convince it that if they stopped whipping the humans 
to death and allowed you to work in shifts. you could get them to work more. 
When it agreed it leaned forward and made a different gesture. One that still 
involved its mandibles but this time also your neck. It didn’t need words. 
The message was clear.

Miraculously you also managed to convince the humans. Soon the mine had increased 
its output (you were never sure by how much. Their words for double and triple sound 
identical to you) it seemed so obvious but your captors didn’t sleep. They 
loved their lives the same way they treated their slaves. They were born, then 
they toiled until they died.

You learned about the escape plot from the <race name. They were so arrogant 
that many of them still didn’t believe any human could understand their language. 
Not knowing how to gauge your loyalty to the captors they had opted to simply 
leave you out.

But you knew when new equipment was arriving. And when visitors were due to 
inspect the mine.

With your help the plan succeeded. You still didn’t think it was possible 
until you witnessed magic for the first time. You escaped into the barren lands 
which had already been stripped clean of resources.

There they taught you to form Umbra from Pale and to wield it. And you joined 
them in the fight to free others.

But you were never any good at it. Sure you could shoot fire from your fingertips 
but it did little more than annoy most of the invading species.

And again you knew there was a better way. By then you had learned that there was 
more than just one species. A lot more. And that some weren’t there to take slaves 
and mine resources but to buy them from the others.

So you persuaded them to trade with humans. You learned more of their languages, 
and with that you learned who traded with whom and more important who hated whom. 
And with that you found allies. You established contracts with species who generally 
stayed out of the fight. But who, for a price, would be willing to help stick it 
to other Daemons.

Other humans tend to look on your “fratrinization” warily. Until a worm thing digs 
them a new base for the cost of dung. 