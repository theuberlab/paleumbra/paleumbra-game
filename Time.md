# Time
Time does not flow the same in each dimension. It generally flows at similar rates but slightly faster or slightly
slower. Usually not a very perceptable amount, a few seconds or minutes difference a day.

There will be some where its a bit more perceptable. More like an hour or two a day. Some Daemons will have problems
due to this. Like missing a childs birthday or an anniversary because their shift here runs through most of that day.
perhaps this makes them extra cruel. Perhaps this provides Opportunities. A daemonologist could perhaps persuade
someone skilled in illusions to provide a replacement for one of these daemons in order for them to travel home for a
childs birthday. In return they are now willing to sign a contract with the daemonologist.

